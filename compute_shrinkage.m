function shrinkage = compute_shrinkage(...
    folder_fit,...
    subgroups,...
    idx_subgroups)
fit_r        = load([folder_fit '/fit.mat']);
nb_animals   = 0;
for idx_subgroup = 1:length(subgroups)
    nb_animals = nb_animals + length(subgroups(idx_subgroup));
end
rand_effs   = zeros(nb_animals, length(fit_r.beta));

comp        = 0;
s           = 1;
for g = idx_subgroups    
    comp_group       = 0; 
    for j = s:s+ length(subgroups{g}) -1        
        comp           = comp + 1;
        comp_group     = comp_group + 1;
        rand_eff       = fit_r.B(:, j);
        rand_effs(comp, :) = rand_eff'; % tutti 
    end
    s = j+1;
    emp_variance = var(rand_effs,0,1);
    diag_psi     = diag(fit_r.psi)';
    shrinkage    = 100*(1 - emp_variance./diag_psi);
end

