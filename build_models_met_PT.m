constants;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vehicles - same growth GompExp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Model_vehicle1.model           = @(param, time, resection_time) model_burden_nb_PT(...  % param: mu, alpha, beta
    [param(1), param(2:3)],...                            % metastatic parameters [mu, growth_parameters]
    time,...                                              % time points
    @(param, time, X0) gompertz_exp_alpha0_beta(...       % growth model for primary tumor
        param,...
        DT_invitro,...
        time,...
        cell2mm(1),... % volume is in mm^3 for the PT
        X0),...
    @(param, time, X0) gompertz_exp_alpha0_beta(...      % growth model for primary tumor
        param,...
        DT_invitro,...
        time,...
        cell2phot(1),... % volume is in bioluminescence for the mets
        X0),... 
    [param(2:3), resection_time],...                      % parameters for primary tumor growth (with resection time being the last one)
    0);
Model_vehicle1.param_names     = {'$\mu$', '$\alpha$', '$\beta$'};
Model_vehicle1.Units           = {'$mm^{-3}\cdot day^{-1}$', '$day^{-1}$', '$day^{-1}$'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Treated. Based on GompExp growth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Model_ther1.model                                = @(param, time, resection_time, t_start, k_e, Ci) model_burden_nb_PT(...  % param: mu, alpha, beta, k 
    [param(1), param(2:3)],...                                                                      % metastatic parameters
    time,...                                                                              % time points
    @(param, time, X0) GompExp_ther(param, time, t_start, DT_invitro, k_e, Ci, cell2mm(1), X0),...          % growth model for primary tumor
    @(param, time, X0) gompertz_exp_alpha0_beta(...      % growth model for primary tumor
        param,...
        DT_invitro,...
        time,...
        cell2phot(1),... % volume is in bioluminescence for the mets
        X0),...                            
    [param(2:4), resection_time],...                                 % parameters for primary tumor growth (with resection time being the last one)
    0);
Model_ther1.param_names                          = {'$\mu$', '$\alpha$', 'beta','$k$'};
Model_ther1.Units                                = {'$mm^{-3}\cdot day^{-1}$', '$day^{-1}$', '$day^{-1}$', '(mg/kg)^{-1}'};