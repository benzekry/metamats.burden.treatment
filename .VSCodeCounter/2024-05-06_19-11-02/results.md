# Summary

Date : 2024-05-06 19:11:02

Directory /Users/sbenzekry/work/metastases/ebos/code_neoadjuvant_breast

Total : 321 files,  24294 codes, 4609 comments, 1609 blanks, all 30512 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| MATLAB | 221 | 11,715 | 3,563 | 674 | 15,952 |
| JSON | 4 | 4,634 | 0 | 4 | 4,638 |
| Python | 33 | 3,387 | 566 | 264 | 4,217 |
| LaTeX | 19 | 1,308 | 84 | 103 | 1,495 |
| Log | 5 | 996 | 0 | 62 | 1,058 |
| R | 11 | 928 | 385 | 277 | 1,590 |
| CSV | 5 | 726 | 0 | 5 | 731 |
| Markdown | 8 | 436 | 0 | 192 | 628 |
| Properties | 9 | 117 | 0 | 9 | 126 |
| Java | 1 | 23 | 7 | 8 | 38 |
| Shell Script | 4 | 15 | 4 | 6 | 25 |
| Docker | 1 | 9 | 0 | 5 | 14 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 321 | 24,294 | 4,609 | 1,609 | 30,512 |
| . (Files) | 40 | 3,313 | 784 | 221 | 4,318 |
| carcinom | 237 | 18,044 | 3,526 | 1,164 | 22,734 |
| carcinom (Files) | 3 | 15 | 1 | 11 | 27 |
| carcinom/carcinom_python | 31 | 2,415 | 459 | 164 | 3,038 |
| carcinom/carcinom_python (Files) | 21 | 1,567 | 302 | 112 | 1,981 |
| carcinom/carcinom_python/fit_analysis | 7 | 617 | 140 | 8 | 765 |
| carcinom/carcinom_python/levenberg | 3 | 231 | 17 | 44 | 292 |
| carcinom/data | 1 | 174 | 0 | 1 | 175 |
| carcinom/matlab | 180 | 8,912 | 2,605 | 625 | 12,142 |
| carcinom/matlab (Files) | 31 | 2,361 | 339 | 87 | 2,787 |
| carcinom/matlab/PK | 1 | 28 | 0 | 0 | 28 |
| carcinom/matlab/fit_sav | 12 | 678 | 12 | 30 | 720 |
| carcinom/matlab/fit_sav/data_example | 6 | 294 | 12 | 25 | 331 |
| carcinom/matlab/fit_sav/data_example_avg | 6 | 384 | 0 | 5 | 389 |
| carcinom/matlab/graphical_tools | 67 | 4,630 | 1,857 | 454 | 6,941 |
| carcinom/matlab/graphical_tools (Files) | 47 | 2,134 | 465 | 113 | 2,712 |
| carcinom/matlab/graphical_tools/export_fig-master | 18 | 2,163 | 1,185 | 281 | 3,629 |
| carcinom/matlab/graphical_tools/print_table | 2 | 333 | 207 | 60 | 600 |
| carcinom/matlab/models_tumor | 30 | 378 | 31 | 13 | 422 |
| carcinom/matlab/numerical_methods | 8 | 186 | 47 | 8 | 241 |
| carcinom/matlab/statistics | 25 | 584 | 303 | 31 | 918 |
| carcinom/matlab/therapy_tumor | 6 | 67 | 16 | 2 | 85 |
| carcinom/mlx_py | 22 | 6,528 | 461 | 363 | 7,352 |
| carcinom/mlx_py (Files) | 7 | 1,314 | 151 | 135 | 1,600 |
| carcinom/mlx_py/archive | 11 | 3,085 | 230 | 176 | 3,491 |
| carcinom/mlx_py/archive (Files) | 7 | 956 | 150 | 124 | 1,230 |
| carcinom/mlx_py/archive/monolix_r_version | 4 | 2,129 | 80 | 52 | 2,261 |
| carcinom/mlx_py/monolix_r_version | 4 | 2,129 | 80 | 52 | 2,261 |
| diagnostic_plots | 2 | 137 | 5 | 14 | 156 |
| fit_PT_met_SAEM | 3 | 366 | 7 | 26 | 399 |
| fit_PT_met_SAEM (Files) | 2 | 77 | 7 | 19 | 103 |
| fit_PT_met_SAEM/.latex | 1 | 289 | 0 | 7 | 296 |
| metamats_burden | 19 | 728 | 205 | 14 | 947 |
| monolix | 17 | 1,288 | 14 | 85 | 1,387 |
| monolix (Files) | 1 | 87 | 8 | 23 | 118 |
| monolix/all_groups_104 | 9 | 1,024 | 6 | 54 | 1,084 |
| monolix/all_groups_66 | 7 | 177 | 0 | 8 | 185 |
| monolix/all_groups_66 (Files) | 3 | 157 | 0 | 4 | 161 |
| monolix/all_groups_66/loglogistic | 4 | 20 | 0 | 4 | 24 |
| monolix/all_groups_66/loglogistic/covariateSearch_COSSAC_LRT | 4 | 20 | 0 | 4 | 24 |
| regression_mu | 3 | 418 | 68 | 85 | 571 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)