%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning(['In the analysis, animals that did not develop a PT and animals that were dead following surgery are removed'...
    ' and NaN was applied when there was no disease at endpoint.'...
    ' This is not done in the master file and might explain discrepancies'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
address_data     = 'ebos_data_breast.xlsx';
experiment_names  = {
    'veh_ab_12_07'
    'veh_ab_12_11'
    'veh_ab_13_06'
    'veh_ab_14_05'
    'veh_ab_14_14'
    'Su120_3D_ab_12_07' 
    'Su120_3D_Su60_4D_ab_12_07'
    'Su120_3D_Su60_8D_ab_12_07'
    'Su120_3D_Su60_11D_ab_12_07'
    'Su60_3D_ab_13_06'
    'Su60_3D_ab_14_14'
    'Su60_7D_ab_13_06'           
    'Su60_14D_ab_12_11'
    'Su60_14D_ab_13_06'
    'Su60_14D_ab_14_14' 
    'Su120_3D_ab_12_11'
    'Su120_3D_ab_14_14'
    'Su120_3D_Su60_11D_ab_12_11' 
    'Su120_3D_Su60_11D_ab_14_14' 
    'Ax100_3D_ab_14_14'          
    'Ax100_14D_ab_12_11'
    'Ax100_14D_ab_14_14'
    'Ax200_3D_ab_14_14'          
    'Ax200_3D_Ax100_11D_ab_14_14'
    };
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link experiments <--> subgroups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
experiments_subgroups_d                                 = containers.Map;
experiments_subgroups_d('veh38')                        = {'veh_ab_12_07'};
experiments_subgroups_d('veh34')                        = {'veh_ab_12_11'
                                                           'veh_ab_13_06'
                                                           'veh_ab_14_05'
                                                           'veh_ab_14_14'};
experiments_subgroups_d('Su120_3D_ab_12_07')             = {'Su120_3D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_4D_ab_12_07')    = {'Su120_3D_Su60_4D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_8D_ab_12_07')    = {'Su120_3D_Su60_8D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_11D_ab_12_07')   = {'Su120_3D_Su60_11D_ab_12_07'};
experiments_subgroups_d('Su60_3D')                      = {'Su60_3D_ab_13_06'
                                                           'Su60_3D_ab_14_14'};
experiments_subgroups_d('Su60_7D_ab_13_06')             = {'Su60_7D_ab_13_06'};
experiments_subgroups_d('Su60_14D')                     = {'Su60_14D_ab_12_11'
                                                           'Su60_14D_ab_13_06'
                                                           'Su60_14D_ab_14_14'};
experiments_subgroups_d('Su120_3D')                     = {'Su120_3D_ab_12_11'
                                                           'Su120_3D_ab_14_14'};
experiments_subgroups_d('Su120_3D_Su60_11D')            = {'Su120_3D_Su60_11D_ab_12_11' 
                                                           'Su120_3D_Su60_11D_ab_14_14'};
experiments_subgroups_d('Ax100_3D_ab_14_14')            = {'Ax100_3D_ab_14_14'};
experiments_subgroups_d('Ax100_14D')                    = {'Ax100_14D_ab_12_11'
                                                           'Ax100_14D_ab_14_14'};
experiments_subgroups_d('Ax200_3D_ab_14_14')            = {'Ax200_3D_ab_14_14'};
experiments_subgroups_d('Ax200_3D_Ax100_11D_ab_14_14')  = {'Ax200_3D_Ax100_11D_ab_14_14'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Survival times (DPI = Days Post Injection)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d = containers.Map;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Vehicles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('veh_ab_12_07') = xlsread(address_data, 'CB61:CB66');
survival_d('veh_ab_12_11') = xlsread(address_data, 'CB93:CB98');
survival_d('veh_ab_13_06') = xlsread(address_data, 'CB131:CB136');
survival_d('veh_ab_14_05') = xlsread(address_data, 'CB158:CB163');
survival_d('veh_ab_14_14') = xlsread(address_data, 'CB178:CB186');
                             
survival_d('veh38')        = survival_d('veh_ab_12_07');
survival_d('veh34')        = [survival_d('veh_ab_12_11'); ...              
                              survival_d('veh_ab_13_06'); ...
                              survival_d('veh_ab_14_05');...               
                              survival_d('veh_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Treated subgroups from ab_12_07
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Su120_3D_ab_12_07')         = xlsread(address_data, 'CB67:CB72');
survival_d('Su120_3D_Su60_4D_ab_12_07') = xlsread(address_data, 'CB73:CB78');
survival_d('Su120_3D_Su60_8D_ab_12_07') = xlsread(address_data, 'CB79:CB84');   
survival_d('Su120_3D_Su60_11D_ab_12_07')= [xlsread(address_data,'CB85:CB86');...
                                           xlsread(address_data,'CB88:CB89')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Su60_3D_ab_13_06')       = [xlsread(address_data,'CB137:CB139');...
                                      NaN; ...
                                      xlsread(address_data,'CB141:CB142')];
survival_d('Su60_3D_ab_14_14')       = xlsread(address_data,'CB187:CB195');
survival_d('Su60_3D')             = [...
    survival_d('Su60_3D_ab_13_06')
    survival_d('Su60_3D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 7D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           
survival_d('Su60_7D_ab_13_06')          = xlsread(address_data,'CB143:CB148');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 14D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Su60_14D_ab_12_11')         = xlsread(address_data,'CB99:CB104');
survival_d('Su60_14D_ab_13_06')         = [xlsread(address_data,'CB149:CB153');NaN];
survival_d('Su60_14D_ab_14_14')         = xlsread(address_data,'CB204:CB212'); 
survival_d('Su60_14D')            = [...
    survival_d('Su60_14D_ab_12_11')         
    survival_d('Su60_14D_ab_13_06')       
    survival_d('Su60_14D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su120 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Su120_3D_ab_12_11')  = xlsread(address_data, 'CB111:CB122');
survival_d('Su120_3D_ab_14_14')  = xlsread(address_data, 'CB196:CB203');
survival_d('Su120_3D')              = [...
   survival_d('Su120_3D_ab_12_11') 
   survival_d('Su120_3D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su120 3D Su60 11D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
survival_d('Su120_3D_Su60_11D_ab_12_11')  = xlsread(address_data, 'CB123:CB128');  
survival_d('Su120_3D_Su60_11D_ab_14_14')  = xlsread(address_data, 'CB213:CB221');
survival_d('Su120_3D_Su60_11D') = [...
    survival_d('Su120_3D_Su60_11D_ab_12_11')
    survival_d('Su120_3D_Su60_11D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax100 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Ax100_3D_ab_14_14') = [xlsread(address_data, 'CB222:CB222');NaN;xlsread(address_data, 'CB224:CB229')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax100 14D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Ax100_14D_ab_12_11') = xlsread(address_data, 'CB105:CB110'); 
survival_d('Ax100_14D_ab_14_14') = xlsread(address_data, 'CB238:CB245');
survival_d('Ax100_14D') = [...
    survival_d('Ax100_14D_ab_12_11')
    survival_d('Ax100_14D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax200 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Ax200_3D_ab_14_14') = xlsread(address_data, 'CB230:CB237');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax200 3D Ax100 11D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival_d('Ax200_3D_Ax100_11D_ab_14_14') = xlsread(address_data, 'CB246:CB253');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% All groups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
survival  = [
    survival_d('veh38')
    survival_d('veh34')
    survival_d('Su120_3D_ab_12_07') 
    survival_d('Su120_3D_Su60_4D_ab_12_07')
    survival_d('Su120_3D_Su60_8D_ab_12_07')
    survival_d('Su120_3D_Su60_11D_ab_12_07')
    survival_d('Su60_3D')
    survival_d('Su60_7D_ab_13_06')           
    survival_d('Su60_14D')
    survival_d('Su120_3D')
    survival_d('Su120_3D_Su60_11D') 
    survival_d('Ax100_3D_ab_14_14')          
    survival_d('Ax100_14D')
    survival_d('Ax200_3D_ab_14_14')          
    survival_d('Ax200_3D_Ax100_11D_ab_14_14')
    ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Censoring
% 1 for censeored data
% da fare, vedi se serve
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% idx_cens                = find(isnan(survival));       
% cens_data               = zeros(length(survival), 1);
% cens_data(idx_cens)     = 1;
% survival(idx_cens(1:3)) = 108;
% survival(idx_cens(4))   = 87;
% survival(idx_cens(5:6)) = 145;
% survival(idx_cens(7))   = 87;
% survival(idx_cens(8))   = 145;
% survivals               = survival;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normalization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
min_value = 3; % min value of all DPI survivals/10 (see Master file)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Control groups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
control_names_d                               = containers.Map;
control_names_d('veh_ab_12_07')               = 'veh_ab_12_07';
control_names_d('veh_ab_12_11')               = 'veh_ab_12_11'; 
control_names_d('veh_ab_13_06')               = 'veh_ab_13_06';
control_names_d('veh_ab_14_05')               = 'veh_ab_14_05'; % not used in master file
control_names_d('veh_ab_14_14')               = 'veh_ab_14_14';
control_names_d('Su120_3D_ab_12_07')          = 'veh_ab_12_07'; 
control_names_d('Su120_3D_Su60_4D_ab_12_07')  = 'veh_ab_12_07';
control_names_d('Su120_3D_Su60_8D_ab_12_07')  = 'veh_ab_12_07';
control_names_d('Su120_3D_Su60_11D_ab_12_07') = 'veh_ab_12_07';
control_names_d('Su60_3D_ab_13_06')           = 'veh_ab_13_06';
control_names_d('Su60_3D_ab_14_14')           = 'veh_ab_14_14';
control_names_d('Su60_7D_ab_13_06')           = 'veh_ab_13_06';          
control_names_d('Su60_14D_ab_12_11')          = 'veh_ab_12_11';
control_names_d('Su60_14D_ab_13_06')          = 'veh_ab_13_06';
control_names_d('Su60_14D_ab_14_14')          = 'veh_ab_14_14'; 
control_names_d('Su120_3D_ab_12_11')          = 'veh_ab_12_11';
control_names_d('Su120_3D_ab_14_14')          = 'veh_ab_14_14';
control_names_d('Su120_3D_Su60_11D_ab_12_11') = 'veh_ab_12_11'; 
control_names_d('Su120_3D_Su60_11D_ab_14_14') = 'veh_ab_14_14'; 
control_names_d('Ax100_3D_ab_14_14')          = 'veh_ab_14_14';          
control_names_d('Ax100_14D_ab_12_11')         = 'veh_ab_12_11';
control_names_d('Ax100_14D_ab_14_14')         = 'veh_ab_14_14';
control_names_d('Ax200_3D_ab_14_14')          = 'veh_ab_14_14';          
control_names_d('Ax200_3D_Ax100_11D_ab_14_14')= 'veh_ab_14_14';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% normalized = log2(survival + min_value) - median(log2(control))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
norm_survival_d = containers.Map;
for idx_key = 1:length(experiment_names)
    key                  = experiment_names{idx_key};
    control_surv         = survival_d(control_names_d(key));
    control_surv         = control_surv(~isnan(control_surv)); % toglie NaN
    median_control       = median(control_surv + min_value);%median(log2(control_surv + min_value));
    norm_survival_d(key) = log2(survival_d(key) +  min_value) - log2(median_control);
end
norm_survivals = [];
for idx = 1:length(subgroup_names)
    C                  = norm_survival_d.values(experiments_subgroups_d(subgroup_names{idx}));
    norm_survival_loc  = [];
    for c = 1:length(C)
        norm_survival_loc = [norm_survival_loc; C{c}];
    end
    norm_survival_d(subgroup_names{idx}) = norm_survival_loc;
    norm_survivals                        = [norm_survivals; norm_survival_loc];
end


