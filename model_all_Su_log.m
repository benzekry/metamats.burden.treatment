function YFIT = model_all_Su_log(param, time, V, model_v, model_t) % param : mu, alpha, beta, ks, ka 
[g,m] = size(V);
param = reshape(param, 1, length(param));
param_v = param(1:3);
param_s = param(1:4);
%param_a = [param(1:3), param(5)];
ke_s    = 3.26;
%ke_a    = 3.85; 
for j = 1:g
    switch V(j)
        case 1 
            YFIT = model_v(param_v, time, 38); 
        case 2
            YFIT = model_v(param_v, time, 34); 
        case 3
            YFIT = model_t(param_s, time, 38, 23, ke_s, repelem(120,3));  % Su120(3D) t23
        case 4
            YFIT = model_t(param_s, time, 38, 23, ke_s, [repelem(120,3), repelem(60,4)]);
        case 5
            YFIT = model_t(param_s, time, 38, 23, ke_s, [repelem(120,3), repelem(60,8)]);
        case 6
            YFIT = model_t(param_s, time, 38, 23, ke_s, [repelem(120,3), repelem(60,11)]);
        case 7
            YFIT = model_t(param_s, time, 34, 20, ke_s, repelem(60,3));
        case 8
            YFIT = model_t(param_s, time, 34, 20, ke_s, repelem(60,7));
        case 9
            YFIT = model_t(param_s, time, 34, 20, ke_s, repelem(60,14));
        case 10
            YFIT = model_t(param_s, time, 34, 20, ke_s, repelem(120,3));
        case 11
            YFIT = model_t(param_s, time, 34, 20, ke_s, [repelem(120,3), repelem(60,11)]);
%         case 12
%             YFIT = model_t(param_s, time, 34, 20, ke_a, repelem(100,3));
%         case 13
%             YFIT = model_t(param_a, time, 34, 20, ke_a, repelem(100,14));
%         case 14
%             YFIT = model_t(param_a, time, 34, 20, ke_a, repelem(200,3));
%         case 15 
%             YFIT = model_t(param_a, time, 34, 20, ke_a, [repelem(200,3), repelem(100,11)]);
    end
    YFIT=log(max(YFIT,realmin));
    
end

