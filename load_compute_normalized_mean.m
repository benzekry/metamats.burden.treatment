function [datas, data_d, norm_datas, norm_data_d] = load_compute_normalized_mean(...
    column,...
    min_value,...
    normalization_function) % median or mean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loads the data from the column <column> of the file <address_data>
% Computes normalized log2 fold changes. Normalization is made relatively
% to the corresponding control group
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning(['In the analysis, animals that were dead following surgery are removed'...
    ' and NaN was applied when there was no disease at endpoint.'...
    ' This is not done in the master file and might explain discrepancies'])
address_data     = 'ebos_data_breast.xlsx';
subgroup_names  = {
    'veh38'
    'veh34'
    'Su120_3D_ab_12_07'          % t_start = 23
    'Su120_3D_Su60_4D_ab_12_07'
    'Su120_3D_Su60_8D_ab_12_07'
    'Su120_3D_Su60_11D_ab_12_07'
    'Su60_3D'                    % AB13_06, AB14_14
    'Su60_7D_ab_13_06'           % AB13_06
    'Su60_14D'                   % AB12_11, AB13_06, AB14_14 
    'Su120_3D'                   % AB12_11, AB14_14
    'Su120_3D_Su60_11D'          % AB12_11, AB14_14
    'Ax100_3D_ab_14_14'          % AB14_14
    'Ax100_14D'                  % AB14_14, AB12_11
    'Ax200_3D_ab_14_14'          % AB14_14
    'Ax200_3D_Ax100_11D_ab_14_14'% AB14_14
    };
experiment_names  = {
    'veh_ab_12_07'
    'veh_ab_12_11'
    'veh_ab_13_06'
    'veh_ab_14_05'
    'veh_ab_14_14'
    'Su120_3D_ab_12_07' 
    'Su120_3D_Su60_4D_ab_12_07'
    'Su120_3D_Su60_8D_ab_12_07'
    'Su120_3D_Su60_11D_ab_12_07'
    'Su60_3D_ab_13_06'
    'Su60_3D_ab_14_14'
    'Su60_7D_ab_13_06'           
    'Su60_14D_ab_12_11'
    'Su60_14D_ab_13_06'
    'Su60_14D_ab_14_14' 
    'Su120_3D_ab_12_11'
    'Su120_3D_ab_14_14'
    'Su120_3D_Su60_11D_ab_12_11' 
    'Su120_3D_Su60_11D_ab_14_14' 
    'Ax100_3D_ab_14_14'          
    'Ax100_14D_ab_12_11'
    'Ax100_14D_ab_14_14'
    'Ax200_3D_ab_14_14'          
    'Ax200_3D_Ax100_11D_ab_14_14'
    };
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Link experiments <--> subgroups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
experiments_subgroups_d                                 = containers.Map;
experiments_subgroups_d('veh38')                        = {'veh_ab_12_07'};
experiments_subgroups_d('veh34')                        = {'veh_ab_12_11'
                                                           'veh_ab_13_06'
                                                           'veh_ab_14_05'
                                                           'veh_ab_14_14'};
experiments_subgroups_d('Su120_3D_ab_12_07')             = {'Su120_3D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_4D_ab_12_07')    = {'Su120_3D_Su60_4D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_8D_ab_12_07')    = {'Su120_3D_Su60_8D_ab_12_07'};
experiments_subgroups_d('Su120_3D_Su60_11D_ab_12_07')   = {'Su120_3D_Su60_11D_ab_12_07'};
experiments_subgroups_d('Su60_3D')                      = {'Su60_3D_ab_13_06'
                                                           'Su60_3D_ab_14_14'};
experiments_subgroups_d('Su60_7D_ab_13_06')             = {'Su60_7D_ab_13_06'};
experiments_subgroups_d('Su60_14D')                     = {'Su60_14D_ab_12_11'
                                                           'Su60_14D_ab_13_06'
                                                           'Su60_14D_ab_14_14'};
experiments_subgroups_d('Su120_3D')                     = {'Su120_3D_ab_12_11'
                                                           'Su120_3D_ab_14_14'};
experiments_subgroups_d('Su120_3D_Su60_11D')            = {'Su120_3D_Su60_11D_ab_12_11' 
                                                           'Su120_3D_Su60_11D_ab_14_14'};
experiments_subgroups_d('Ax100_3D_ab_14_14')            = {'Ax100_3D_ab_14_14'};
experiments_subgroups_d('Ax100_14D')                    = {'Ax100_14D_ab_12_11'
                                                           'Ax100_14D_ab_14_14'};
experiments_subgroups_d('Ax200_3D_ab_14_14')            = {'Ax200_3D_ab_14_14'};
experiments_subgroups_d('Ax200_3D_Ax100_11D_ab_14_14')  = {'Ax200_3D_Ax100_11D_ab_14_14'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Survival times (DPI = Days Post Injection)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d = containers.Map;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Vehicles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('veh_ab_12_07') = xlsread_nan(address_data, [column '61:' column '66']);
data_d('veh_ab_12_11') = xlsread_nan(address_data, [column '93:' column '98']);
data_d('veh_ab_13_06') = xlsread_nan(address_data, [column '131:' column '136']);
data_d('veh_ab_14_05') = xlsread_nan(address_data, [column '158:' column '163']);
data_d('veh_ab_14_14') = xlsread_nan(address_data, [column '178:' column '186']);
                             
data_d('veh38')        = data_d('veh_ab_12_07');
data_d('veh34')        = [...
    data_d('veh_ab_12_11'); ...              
    data_d('veh_ab_13_06'); ...
    data_d('veh_ab_14_05');...               
    data_d('veh_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Treated subgroups from ab_12_07
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Su120_3D_ab_12_07')         = xlsread_nan(address_data, [column '67:' column '72']);
data_d('Su120_3D_Su60_4D_ab_12_07') = xlsread_nan(address_data, [column '73:' column '78']);
data_d('Su120_3D_Su60_8D_ab_12_07') = xlsread_nan(address_data, [column '79:' column '84']);
data_d('Su120_3D_Su60_11D_ab_12_07')= [xlsread_nan(address_data,[column '85:' column '86']);...
                                           xlsread_nan(address_data, [column '88:' column '89'])];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Su60_3D_ab_13_06')       = [xlsread_nan(address_data,[column '137:' column '139']);...
                                      NaN; ...
                                      xlsread_nan(address_data,[column '141:' column '142'])];
data_d('Su60_3D_ab_14_14')       = xlsread_nan(address_data, [column '187:' column '195']);
data_d('Su60_3D')             = [...
    data_d('Su60_3D_ab_13_06')
    data_d('Su60_3D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 7D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           
data_d('Su60_7D_ab_13_06')          = xlsread_nan(address_data,[column '143:' column '148']);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su60 14D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Su60_14D_ab_12_11')         = xlsread_nan(address_data,[column '99:' column '104']);
data_d('Su60_14D_ab_13_06')         = [xlsread_nan(address_data,[column '149:' column '153']);NaN];
data_d('Su60_14D_ab_14_14')         = xlsread_nan(address_data,[column '204:' column '212']); 
data_d('Su60_14D')            = [...
    data_d('Su60_14D_ab_12_11')         
    data_d('Su60_14D_ab_13_06')       
    data_d('Su60_14D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su120 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Su120_3D_ab_12_11')  = xlsread_nan(address_data, [column '111:' column '122']);
data_d('Su120_3D_ab_14_14')  = xlsread_nan(address_data, [column '196:' column '203']);
data_d('Su120_3D')              = [...
   data_d('Su120_3D_ab_12_11') 
   data_d('Su120_3D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Su120 3D Su60 11D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
data_d('Su120_3D_Su60_11D_ab_12_11')  = xlsread_nan(address_data, [column '123:' column '128']);  
data_d('Su120_3D_Su60_11D_ab_14_14')  = xlsread_nan(address_data, [column '213:' column '221']);
data_d('Su120_3D_Su60_11D') = [...
    data_d('Su120_3D_Su60_11D_ab_12_11')
    data_d('Su120_3D_Su60_11D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax100 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Ax100_3D_ab_14_14') = [xlsread_nan(address_data, [column '222:' column '222']);NaN;xlsread_nan(address_data, [column '224:' column '229'])];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax100 14D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Ax100_14D_ab_12_11') = xlsread_nan(address_data, [column '105:' column '110']); 
data_d('Ax100_14D_ab_14_14') = xlsread_nan(address_data, [column '238:' column '245']);
data_d('Ax100_14D') = [...
    data_d('Ax100_14D_ab_12_11')
    data_d('Ax100_14D_ab_14_14')];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax200 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Ax200_3D_ab_14_14') = xlsread_nan(address_data, [column '230:' column '237']);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ax200 3D Ax100 11D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_d('Ax200_3D_Ax100_11D_ab_14_14') = xlsread_nan(address_data, [column '246:' column '253']);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% All groups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datas  = [
    data_d('veh38')
    data_d('veh34')
    data_d('Su120_3D_ab_12_07') 
    data_d('Su120_3D_Su60_4D_ab_12_07')
    data_d('Su120_3D_Su60_8D_ab_12_07')
    data_d('Su120_3D_Su60_11D_ab_12_07')
    data_d('Su60_3D')
    data_d('Su60_7D_ab_13_06')           
    data_d('Su60_14D')
    data_d('Su120_3D')
    data_d('Su120_3D_Su60_11D') 
    data_d('Ax100_3D_ab_14_14')          
    data_d('Ax100_14D')
    data_d('Ax200_3D_ab_14_14')          
    data_d('Ax200_3D_Ax100_11D_ab_14_14')
    ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normalization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Control groups
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
control_names_d                               = containers.Map;
control_names_d('veh_ab_12_07')               = 'veh_ab_12_07';
control_names_d('veh_ab_12_11')               = 'veh_ab_12_11'; 
control_names_d('veh_ab_13_06')               = 'veh_ab_13_06';
control_names_d('veh_ab_14_05')               = 'veh_ab_14_05'; % not used in master file
control_names_d('veh_ab_14_14')               = 'veh_ab_14_14';
control_names_d('Su120_3D_ab_12_07')          = 'veh_ab_12_07'; 
control_names_d('Su120_3D_Su60_4D_ab_12_07')  = 'veh_ab_12_07';
control_names_d('Su120_3D_Su60_8D_ab_12_07')  = 'veh_ab_12_07';
control_names_d('Su120_3D_Su60_11D_ab_12_07') = 'veh_ab_12_07';
control_names_d('Su60_3D_ab_13_06')           = 'veh_ab_13_06';
control_names_d('Su60_3D_ab_14_14')           = 'veh_ab_14_14';
control_names_d('Su60_7D_ab_13_06')           = 'veh_ab_13_06';          
control_names_d('Su60_14D_ab_12_11')          = 'veh_ab_12_11';
control_names_d('Su60_14D_ab_13_06')          = 'veh_ab_13_06';
control_names_d('Su60_14D_ab_14_14')          = 'veh_ab_14_14'; 
control_names_d('Su120_3D_ab_12_11')          = 'veh_ab_12_11';
control_names_d('Su120_3D_ab_14_14')          = 'veh_ab_14_14';
control_names_d('Su120_3D_Su60_11D_ab_12_11') = 'veh_ab_12_11'; 
control_names_d('Su120_3D_Su60_11D_ab_14_14') = 'veh_ab_14_14'; 
control_names_d('Ax100_3D_ab_14_14')          = 'veh_ab_14_14';          
control_names_d('Ax100_14D_ab_12_11')         = 'veh_ab_12_11';
control_names_d('Ax100_14D_ab_14_14')         = 'veh_ab_14_14';
control_names_d('Ax200_3D_ab_14_14')          = 'veh_ab_14_14';          
control_names_d('Ax200_3D_Ax100_11D_ab_14_14')= 'veh_ab_14_14';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% normalized = log2(survival + min_value) - median(log2(control))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
norm_data_d = containers.Map;
for idx_key = 1:length(experiment_names)
    key             = experiment_names{idx_key};
    control         = data_d(control_names_d(key));
    control         = control(~isnan(control));
    
    if strcmp(normalization_function, 'mean')
        %control_    = log2(mean(control + min_value));  ?    
        control_    = mean(log2(control + min_value)); 
    elseif strcmp(normalization_function, 'median')
        %control_    = log2(median(control + min_value)); 
        control_    = median(log2(control + min_value));
    end
    norm_data_d(key) = log2(data_d(key) +  min_value) - control_;
    
end
norm_datas = [];
for idx = 1:length(subgroup_names)
    C                                = norm_data_d.values(experiments_subgroups_d(subgroup_names{idx}));
    norm_data_d(subgroup_names{idx}) = vertcat(C{:});    
    norm_datas                   = vertcat(norm_datas, vertcat(C{:}));
end
