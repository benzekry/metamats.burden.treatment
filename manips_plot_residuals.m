clear all
close all
clc

folder          = './fit_PT_met_SAEM';
fit_r           = load([folder '/fit.mat']);


iwres = fit_r.stats.iwres;

colors       = get(gca,'ColorOrder');

% Scatter plots of the residuals
type_data = [];
for j = 1:fit_r.N
    data_met_loc = fit_r.data_met{j};
    time_met_loc = fit_r.time_met{j};
    time_PT_loc  = fit_r.time_PT{j};
    data_PT_loc  = fit_r.data_PT{j};
    n_PT         = length(data_PT_loc);
    n_met        = length(data_met_loc);
    PT           = repmat(0, n_PT,1);
    met          = repmat(1,n_met,1);
    type_data    = [type_data;PT;met];
end

X       = fit_r.X;
pred_PT = fit_r.ind_pred_PT;
pred_met= fit_r.ind_pred_met;
T = table(X, type_data, iwres);
T_PT  = T(T.type_data == 0, {'X','iwres'});
T_PT  = [T_PT table(pred_PT)];
T_met = T(T.type_data == 1, {'X','iwres'}); 
T_met = [T_met table(pred_met)];

plot(T_PT.X, T_PT.iwres, 'o', 'Color', colors(1,:,:))
hline = refline([0 0]);
hline.Color = 'k';
hline.LineStyle = '--';
ylim([-6 6])
xlabel('Time')
ylabel('IWRES')
set_fonts_lines_diag(gca)
export_fig('./diagnostic_plots/scatter_iwres_PT.pdf')

close all
plot(log(T_PT.pred_PT), T_PT.iwres, 'o', 'Color', colors(1,:,:))
xlim([17,22])
ylim([-6,6])
hline = refline([0 0]);
hline.Color = 'k';
hline.LineStyle = '--';
xlabel('log(Predictions)')
ylabel('IWRES')
set_fonts_lines_diag(gca)
export_fig('./diagnostic_plots/scatter_iwres_VS_log_predictions_PT.pdf')
 
close all
plot(log(T_met.pred_met), T_met.iwres, 'o', 'Color', colors(2,:,:))
xlabel('log(Predictions)')
ylabel('IWRES')
%xlim([-0.1e9,max(T_met.pred_met) + 0.1e9])
ylim([-6,6])
hline = refline([0 0]);
hline.Color = 'k';
hline.LineStyle = '--';
set_fonts_lines_diag(gca)
export_fig('./diagnostic_plots/scatter_iwres_VS_log_predictions_met.pdf')

close all
plot(T_met.X, T_met.iwres, 'o', 'Color', colors(2,:,:))
hline = refline([0 0]);
hline.Color = 'k';
hline.LineStyle = '--';
xlabel('Time')
ylabel('IWRES')
ylim([-6,6]) % 
set_fonts_lines_diag(gca)
export_fig('./diagnostic_plots/scatter_iwres_met.pdf')




