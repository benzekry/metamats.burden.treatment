clear all
close all
clc

constants;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_name_PT    = 'PT';
data_name_met   = 'met';
folder_glob     = './ther_mets/fit_veh2016';
folder          = './ther_mets/simu_ther_met9';
conv_unit_PT    = mm2cell;
conv_unit_met   = phot2cell;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fit_r               = load([folder_glob '/fit.mat']);
fixed_effects_PT    = fit_r.beta(1:2); % should be changed if the model is changed
fixed_effects_met   = fit_r.beta(1:3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
max_time        = 85;
temps_met       = 0:0.1:max_time;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_set_PT         = evalin('base', data_name_PT);
time_PT             = evalin('base', ['time_' data_name_PT]);
data_set_met        = evalin('base', data_name_met);
time_met            = evalin('base', ['time_' data_name_met]);
index_postresection = 1; %First time index post-resection (retains only the metastatic burden post-resection)
max_size            = 2e10;
min_size            = 1e2;
plot_names = {
    'veh38'
    'veh34'
    'Su120_3D_ab_12_07' % t_start = 23
    'Su120_3D_Su60_4D_ab_12_07'
    'Su120_3D_Su60_8D_ab_12_07'
    'Su120_3D_Su60_11D_ab_12_07'
    'Su60_3D'           % AB13_06, AB14_14
    'Su60_7D_ab_13_06'  % AB13_06
    'Su60_14D_ther_prova'          % AB12_11, AB13_06, AB14_14 
    'Su120_3D'          % AB12_11, AB14_14
    'Su120_3D_Su60_11D' % AB12_11, AB14_14
    };
param_PT    = exp(fixed_effects_PT);       % for simulation of ther on mets
param_met   = param_PT;
for g = 9
    temps_PT        = 0:0.1:resection_time(g);
    model_met_f         = @(param, time, param_PT) model_burden_nb(...
        [param(1), param_PT(1:2)],...
        time,...
        @(param, time, X0) gompertz_exp_alpha0_beta_ther_gr0(param, DT_invitro, time, cell2mm(1), 0, X0, t_starts(g), t_ends(g)),...
        @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
        param_PT,...
        0);
    model_PT_f          = @(param, time)gompertz_exp_alpha0_beta_ther_gr0(param, DT_invitro, time, cell2mm(1), 0, Vinj, t_starts(g), t_ends(g));
    growth_model_ther   =  @(param, time, tI, VI)gompertz_exp_alpha_beta_ther_gr0(param, DT_invitro, time, tI, VI, t_starts(g), t_ends(g));


    time_PT_subgroup         = time_PT(subgroups{g});
    data_set_PT_subgroup     = data_set_PT(subgroups{g});
    time_met_subgroup        = time_met(subgroups{g});
    data_set_met_subgroup    = data_set_met(subgroups{g});
%     [AX, hDataPT, hDataMet]  = plot_data_global_conv_unit(...
%         index_postresection,...
%         time_PT_subgroup,...
%         data_set_PT_subgroup,...
%         time_met_subgroup, data_set_met_subgroup, ...
%         {[0, max_time, min_size, max_size], [0, max_time, min_size, max_size]},...
%         conv_unit_PT,...
%         conv_unit_met);
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate population
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nb_simu  = 10000;
pops_PT  = zeros(nb_simu, length(temps_PT));
pops_met = zeros(nb_simu, length(temps_met));
rng(0) % for reproducibility
%t = cputime;
for s = 1:nb_simu
    s
    rng(s)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Randomly generate parameters and deal with lognormal distribution
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    param_loc = exp(mvnrnd(fit_r.beta,  fit_r.psi)');
    % Mets 
    param_loc_met  = [param_loc(3);param_loc(1:2)];
    param_loc_met  = reshape(param_loc_met, 1, length(param_loc_met));
    % PT
    param_loc_PT  = param_loc(1:2);
    param_loc_PT  = reshape(param_loc_PT, 1, length(param_loc_PT));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Simulate model
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [~, ~, burden, Xp]    = model_met_ther_sia(...
    max_time,...                      % final time
    0.1,...                           % discretization step
    growth_model_ther,...             % PT growth
    growth_model_ther,...
    param_loc_PT,...                      % PT growth parameters
    param_loc_PT,...                     % mets growth parameters
    Vinj,... % mm3
    cell2mm(1),... 
    [param_loc_met(1), 1],... % dissemination parameters
    resection_time(g),...
    folder,... % folder for results exportation
    0); % decides whether secondary mets or not
    pops_met(s, :) = burden;
    save([folder '/burden.mat'],'pops_met') 
end
   
%cputime-t
end

