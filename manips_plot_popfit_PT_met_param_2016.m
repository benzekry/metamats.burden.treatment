%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script plots fitted models to different groups of treated animals, 
% using growth and dissemination parameters from untreated animals reported 
% in [Benzekry et al., Cancer Res, 2016] (Scenario B)
% It generates the plots of Figure 1C and S1B of the manuscript
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

constants;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load secondary libraries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath("carcinom"))
addpath(genpath("metamats_burden"))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and build models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
build_models_met_PT;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_name_PT    = 'PT';
data_name_met   = 'met';
folder_fit      = './ther_mets/fit_veh2016';
folder_output   = './ther_mets';
conv_unit_PT    = mm2cell;
conv_unit_met   = phot2cell;
compute_pop     = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fit_r               = load([folder_fit '/fit.mat']);
fixed_effects_PT    = fit_r.beta(1:2); % should be changed if the model is changed
covariance_PT       = fit_r.psi(1:2, 1:2);
param_transform_PT  = fit_r.paramTransform(1:2);
fixed_effects_met   = fit_r.beta(1:3);
covariance_met      = fit_r.psi(1:3, 1:3);
param_transform_met = fit_r.paramTransform(1:3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
max_time        = 85;
temps_met       = 0:0.1:max_time;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define models and plot data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_set_PT         = evalin('base', data_name_PT);
time_PT             = evalin('base', ['time_' data_name_PT]);
data_set_met        = evalin('base', data_name_met);
time_met            = evalin('base', ['time_' data_name_met]);
index_postresection = 1; %First time index post-resection (retains only the metastatic burden post-resection)
max_size            = 2e10;
min_size            = 1e4;
plot_names = {
    'veh38'
    'veh34'
    'Su120_3D_ab_12_07' % t_start = 23
    'Su120_3D_Su60_4D_ab_12_07'
    'Su120_3D_Su60_8D_ab_12_07'
    'Su120_3D_Su60_11D_ab_12_07'
    'Su60_3D'           % AB13_06, AB14_14
    'Su60_7D_ab_13_06'  % AB13_06
    'Su60_14D'          % AB12_11, AB13_06, AB14_14
    'Su120_3D'          % AB12_11, AB14_14
    'Su120_3D_Su60_11D' % AB12_11, AB14_14
    };
k_e = 3.26;
for g = 1:length(plot_names) % 9 % only Figure 1C right
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    time_PT_subgroup         = time_PT(subgroups{g});
    data_set_PT_subgroup     = data_set_PT(subgroups{g});
    time_met_subgroup        = time_met(subgroups{g});
    data_set_met_subgroup    = data_set_met(subgroups{g});
    [AX, hDataPT, hDataMet]  = plot_data_global_conv_unit(...
        index_postresection,...
        time_PT_subgroup,...
        data_set_PT_subgroup,...
        time_met_subgroup, data_set_met_subgroup, ...
        {[0, max_time, min_size, max_size], [0, max_time, min_size, max_size]},...
        conv_unit_PT,...
        conv_unit_met);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Simulate population
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    temps_PT        = 0:0.1:resection_time(g);    
    model_met_f         = @(param, time, param_PT) model_burden_nb(...
        [param(1),  param_PT],...
        time,...
        @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2mm(1), X0), ...
        @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2phot(1), X0),...
        param_PT,...
        0);
    model_PT_f               = @(param, time) gompertz_exp_alpha0_beta(...
        param,...
        DT_invitro,...
        time,...
        cell2mm(1),...
        Vinj);        
    nb_simu                  = 1000;
    if compute_pop == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Simulate population
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        pops_PT  = zeros(nb_simu, length(temps_PT));
        pops_met = zeros(nb_simu, length(temps_met));
        rng(0) % for reproducibility
        for s = 1:nb_simu
            rng(s)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Randomly generate parameters and deal with lognormal distribution
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            param_loc      = exp(mvnrnd(fit_r.beta,  fit_r.psi)');
            param_loc(1)   = param_loc(1) - param_loc(2) *log(cell2mm(1));
            % Mets
            param_loc_met  = [param_loc(3); param_loc(1:2)];
            param_loc_met  = reshape(param_loc_met, 1, length(param_loc_met));
            % PT
            param_loc_PT   = param_loc(1:2);
            param_loc_PT   = reshape(param_loc_PT, 1, length(param_loc_PT));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Simulate model
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            pops_PT(s, :)  = model_PT_f(param_loc_PT, temps_PT);
            pops_met(s, :) = model_met_f(...
                param_loc_met,...
                temps_met,...
                [param_loc_PT, resection_time(g)]);
        end
        if ~exist([folder_output '/' subgroup_names{g}], 'dir')
            mkdir([folder_output '/' subgroup_names{g}])
        end
        save([folder_output '/' subgroup_names{g} '/pops_PT_met'], 'temps_PT', 'temps_met', 'pops_PT', 'pops_met');
    else
        load([folder_output '/' subgroup_names{g} '/pops_PT_met'],  'pops_PT', 'pops_met');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot model simulation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pops_PT                    = conv_unit_PT(pops_PT);
    pops_met                   = conv_unit_met(pops_met);
    flag_plot_met_distribution = 1;
    file                       = [folder_output '/' subgroup_names{g}]; % path to the output file
    plot_PT_met_model(...
        AX,...
        temps_PT,...
        temps_met,...
        pops_PT,...
        pops_met,...
        resection_time(g),...
        min_size,...
        max_size,...
        max_time,...
        flag_plot_met_distribution,...
        t_starts(g),...
        t_ends(g),...
        file);
    close(gcf)
end

