clear all
close all
clc

constants;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and build models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
build_models_met_PT;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_name_PT    = 'PT';
data_name_met   = 'met';
folder_glob     = './fit_PT_met_SAEM';
conv_unit_PT    = mm2cell;
conv_unit_met   = phot2cell;
i_remove        = [1:6, 34:55, 133:170]; % keep only those used in the fit
z_              = 1.645; % for 90% prediction interval
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fit_r               = load([folder_glob '/fit.mat']);
fixed_effects_PT    = fit_r.beta(2:4); 
fixed_effects_met   = fit_r.beta(1:3);
k_e                 = 3.26;
err                 = fit_r.stats.errorparam;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_set_PT         = evalin('base', data_name_PT);
time_PT             = evalin('base', ['time_' data_name_PT]);
data_set_met        = evalin('base', data_name_met);
time_met            = evalin('base', ['time_' data_name_met]);

data_set_PT(:, i_remove) = [];            
time_PT(:, i_remove)     = [];                
data_set_met(:, i_remove)= [];
time_met(:, i_remove)    = [];

min_size            = 1e4;

param_indS  = [];

colors       = get(gca,'ColorOrder');
outliers_PT  = 0;
outliers_met = 0;
N_PT  = 0;
N_met = 0;
s = 1;
ind_pred_PT  = [];
ind_pred_met = [];
for g = [2,7:11] 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Models definition
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    temps_PT        = 0:0.1:resection_time(g);
    switch g
        case {1,2}
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1),  param_PT],...
                time,...
                @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2phot(1), X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2mm(1), Vinj);
        case 3
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, repelem(120, 3), cell2mm(1), X0), ... 
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g), DT_invitro,k_e, repelem(120, 3),cell2mm(1),Vinj);
        case 4
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,4)], cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,4)],cell2mm(1),Vinj);
       case 5
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,8)], cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,8)],cell2mm(1),Vinj);
       case 6
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,11)], cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g), DT_invitro,k_e, [repelem(120,3), repelem(60,11)],cell2mm(1),Vinj); 
      case 7
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, repelem(60,3), cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, repelem(60,3),cell2mm(1),Vinj);  
     case 8
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g), DT_invitro,k_e, repelem(60,7), cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g), DT_invitro,k_e, repelem(60,7),cell2mm(1),Vinj);
     case 9
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, repelem(60,14), cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro, time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g), DT_invitro, k_e, repelem(60,14),cell2mm(1),Vinj);
     case 10
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g), DT_invitro,k_e, repelem(120,3), cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro, time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param, time, t_starts(g), DT_invitro, k_e, repelem(120,3),cell2mm(1),Vinj);
     case 11
            model_met_f         = @(param, time, param_PT) model_burden_nb(...
                [param(1), param_PT],...
                time,...
                @(param, time, X0) GompExp_ther(param, time, t_starts(g),DT_invitro, k_e, [repelem(120,3), repelem(60,11)], cell2mm(1), X0), ...
                @(param, time, X0) gompertz_exp_alpha0_beta(param,DT_invitro,time,cell2phot(1),X0),...
                param_PT,...
                0);
            model_PT_f          = @(param, time) GompExp_ther(param,time, t_starts(g), DT_invitro, k_e, [repelem(120,3), repelem(60,11)],cell2mm(1),Vinj);  
    end
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Loop over animals in subgroup
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    N_subgroup = length(subgroups{g});    
    for j = s: s + N_subgroup - 1
        time_PT_ind  = time_PT{j};
        PT_ind       = conv_unit_PT(data_set_PT{j});
        time_met_ind = time_met{j};
        met_ind      = conv_unit_met(data_set_met{j});
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Animal parameters and model computation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        param_ind                  = exp(fit_r.beta + fit_r.B(:,j));
        param_ind_PT               = param_ind(2:4);
        PT_size                    = conv_unit_PT(model_PT_f(param_ind_PT, time_PT_ind));
        param_ind_met              = param_ind(1:3);
        param_ind_PT               = reshape(param_ind_PT,1,length(param_ind_PT));
        met_burden                 = conv_unit_met(model_met_f(param_ind_met,time_met_ind,[param_ind_PT, resection_time(g)]));
        ind_pred_PT  = [ind_pred_PT; PT_size'];
        ind_pred_met = [ind_pred_met; met_burden'];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Plot obs vs individual preds
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % loglog scale
%         loglog(PT_ind, PT_size, 'o', 'Color', colors(1, :));
%         hold on
        loglog(met_ind, met_burden, 'o', 'Color', colors(2, :));
        hold on
        outliers_PT  = outliers_PT + sum(log(PT_ind) < log(PT_size)*(1-z_*err)) + sum(log(PT_ind) > log(PT_size)*(1+z_*err));
        outliers_met = outliers_met + sum(log(met_ind) < log(met_burden)*(1-z_*err)) + sum(log(met_ind) > log(met_burden)*(1+z_*err));
        N_PT = N_PT + length(PT_ind);
        N_met = N_met + length(met_ind);
    end
    s = j+1;
end
xLimits = get(gca,'XLim');  
yLimits = get(gca,'YLim');
h(1) = refline(1,0);
h(1).Color = 'k';
h(1).LineWidth = 0.75;
xlabel('Individual predictions')
ylabel('Observations')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add 90% prediction interval 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% PT
% yy  = [1e7:(1e10-1e7)/100:1e10];
% h(2) = loglog(yy, exp(log(yy) * (1 + z_*err)),'--', 'Color', 'k');
% hold on
% loglog(yy,exp(log(yy) * (1 - z_*err)),'--', 'Color', 'k');
% axis([1e7 1e10 1e7 1e10])
% 
% outliers_proportion_PT = 100*outliers_PT/N_PT;
% outliers_proportion_met = 100*outliers_met/N_met;
% outliers_proportion = 100*(outliers_PT+outliers_met)/(N_PT+N_met);
% dim = [.15 .6 .3 .3];
% str = ['Outliers proportion: ', num2str(outliers_proportion_PT,3), '%' ];
% annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w', 'FontSize', 15);
% 
% set_fonts_lines_diag(gca)
% export_fig('./diagnostic_plots/obs_vs_ind_preds_PT_loglog.pdf')

%% Met
yy  = [1e4:(1e10-1e4)/100:1e10];
h(2) = loglog(yy, exp(log(yy) * (1 + z_*err)),'--', 'Color', 'k');
hold on
loglog(yy,exp(log(yy) * (1 - z_*err)),'--', 'Color', 'k');

outliers_proportion_PT = 100*outliers_PT/N_PT;
outliers_proportion_met = 100*outliers_met/N_met;
outliers_proportion = 100*(outliers_PT+outliers_met)/(N_PT+N_met);
dim = [.15 .6 .3 .3];
str = ['Outliers proportion: ', num2str(outliers_proportion_met,3), '%' ];
annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w', 'FontSize', 15);

set_fonts_lines_diag(gca)
export_fig('./diagnostic_plots/obs_vs_ind_preds_met_loglog.pdf')
