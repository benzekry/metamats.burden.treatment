function V = GompExp_ther(param, time, t_start, DT_invitro, k_e, Ci, Vc, Vi) 
% param: alpha0, beta, k
alpha0 = param(1); % relative growth rate at size V0 = 1 cell
beta   = param(2);
k      = param(3);
DT_invitro = DT_invitro/24; % conversion in days
lambda     = log(2)/DT_invitro;
time_eg    = log((Vc/Vi)*exp((alpha0-lambda)/beta))/lambda;
V_eg       = Vi*exp(lambda*time_eg);
time_exp   = time(time <= time_eg);
time_gomp  = time(time > time_eg);
if t_start > time_eg
   V_exp        = Vi*exp(lambda*time_exp);
   t_start_gomp = max(time(1), time_eg); % caution! switch time to Gompertz could occur before initial time, in which case one should take IC to time(1)
   V_start_gomp = max(Vi, V_eg);
   V_gomp       = model_ther_tIVI(param, time_gomp, t_start, k_e, Ci, Vc, t_start_gomp, V_start_gomp);
   V_exp        = reshape(V_exp, length(V_exp),1);
   V_gomp       = reshape(V_gomp, length(V_gomp),1);
   V            = [V_exp; V_gomp];
else
    V = zeros(1,length(time));
end
V = reshape(V,size(time));
end
