function data = xlsread_nan(file, range)
%   import data from an exel file assigning to NaN to empty cells 
[num,txt,raw] = xlsread(file, range);
raw(cellfun(@ischar,raw)) = {NaN};
data = cell2mat(raw);
end

