function V = model_ther(param, time, t_start, k_e, Ci, Vc, Vi) % param: alpha0, beta, k
alpha0 = param(1); % relative growth rate at size V0 = 1 cell
beta   = param(2);
k      = param(3);
M      = length(Ci);
Ci     = reshape(Ci, 1, M);
tau    = t_start:1:t_start + M - 1; % administration times
tau    = reshape(tau,size(Ci));

A      = zeros(size(M+1));
y0     = zeros(size(M+1));
y      = cell(M+1,1); 

% per ora tI = 0, t_start > 0
y0(1)             = log(Vi/Vc);
A(1)              = beta * y0(1) - alpha0;
y{1}              = (A(1)*exp(-beta*time(time <= tau(1))) + alpha0)/beta;
for m = 2:M
    D     = tau(m-1) + (k/k_e)*(Ci(1:m-2) * exp(-k_e * (tau(m-1) - tau(1:m-2)))');
    y0(m) = (alpha0 + A(m-1)*exp(-beta*D))/beta;    %y{m-1}(end);
    D     = tau(m-1) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (tau(m-1) - tau(1:m-1)))');
    A(m)  = (beta * y0(m) - alpha0)/exp(-beta*D);
    idx_m = find((time > tau(m-1)) & (time <= tau(m)));
    idx_m = reshape(idx_m, 1, length(idx_m));
    if ~isempty(idx_m)
        for n = idx_m
            D  = time(n) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (time(n) - tau(1:m-1)))');
            yy = (A(m)*exp(-beta*D) +alpha0)/beta;
            y{m} = [y{m}, yy];
        end
    end
end
D       = tau(M) + (k/k_e)*(Ci(1:M-1) * exp(-k_e * (tau(M) - tau(1:M-1)))');
y0(M+1) = (alpha0 + A(M)*exp(-beta*D))/beta;        %y{M}(end);
D       = tau(M) + (k/k_e)*(Ci(1:M) * exp(-k_e * (tau(M) - tau))');
A(M+1)  = (beta * y0(M+1) - alpha0)/exp(-beta*D);
idx_m   = find(time > tau(M));
idx_m   = reshape(idx_m, 1, length(idx_m));
if ~isempty(idx_m)
    for n = idx_m
        D  = time(n) + (k/k_e)*(Ci(1:M) * exp(-k_e * (time(n) - tau))');
        yy = (A(M+1)*exp(-beta*D) +alpha0)/beta;
        y{M+1} = [y{M+1}, yy];
    end
end

V = [];
for p =1:M+1
    y{p} = reshape(y{p}, 1, length(y{p}));
    V = [V,y{p}];
end
V = Vc *exp(V);
if any(~isfinite(V))
    warning('Model returns Inf value, model set to 0')
    V = zeros(1,length(V));
end
V = reshape(V,size(time));
end
