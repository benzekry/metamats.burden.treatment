options.color_area = [0.85, 0.85, 0.85];
line_style_MFS      = '-o';
hf = figure(4);
clf
ha = axes();
h = plot(1, 1, '-', 'Linewidth', 2);
h.Visible = 'off';
hold on
h = plot(1, 1, line_style_MFS, 'Linewidth', 2);
h.Visible = 'off';

patch = fill(...    
    [0.2, 0.6, 0.6, 0.2],...
    [0, 0, 0.5, 0.5],...
    options.color_area);
set(patch, 'edgecolor', 'none');
set(patch, 'FaceAlpha', 0.5, 'Visible', 'off');
hold off
ha.Visible  = 'off';
Legende     = {'Primary tumor', 'Metastatic free survival', '90% of population'};
h_leg       = legend(Legende);
set(h_leg, 'EdgeColor', 'w')
set(hf, 'Color', 'w')
