#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 17:28:57 2017

@author: chiara
"""

import numpy as np
from scipy.io import loadmat
from scipy.stats import norm
import pandas as pd


filename  = 'fit.mat'

mat       = loadmat(filename)
param_pop = mat['param_pop'][0]
Covb      = mat['stats']['covb'][0,0]
sebeta    = mat['stats']['sebeta'][0,0][:,0]
psi       = mat['psi']
rse       = mat['rse'][0]
param_est = [param_pop[0], param_pop[1], param_pop[2], param_pop[3], '3.26 (fixed)']
CIm       = mat['CIm'][0]
CIp       = mat['CIp'][0]
CI        = []
CV        = mat['CV'][0]

# units for mu
param_est[0] = param_est[0]*1e-06
CIm[0]       = CIm[0]*1e-06
CIp[0]       = CIp[0]*1e-06
for n in range(4):
	# CI
    if CIp[n] > CIm[n]:
        s = '(%.3g, %.3g)' % (CIm[n], CIp[n])
    else:
        s = '(%.3g, %.3g)' % (CIp[n], CIm[n])
    CI.append(s)
CI.append('-')


median_value = []
for n in range(4):
    s = '%.3g' % param_est[n]
    median_value.append(s)
median_value.append('3.26 (fixed)')



CV_s = []
for n in range(4):
    s = '%.3g' % CV[n]
    CV_s.append(s)
CV_s.append('-')

rse_s = []
for n in range(4):
    s = '%.3g' % rse[n]
    rse_s.append(s)
rse_s.append('-')

param_names = ['$\\mu$','$ \\alpha$', '$\\beta$', '$k$','$k_e$' ]
unit        = ['$cell^{-1}\cdot day^{-1}$', '$day^{-1}$', '$day^{-1}$', '$(mg/kg)^{-1}$', '$day^{-1}$']


d   = {'Median value': median_value, 'Unit':unit ,\
	   'CV (\%)': CV_s,\
       'r.s.e. (\%)': rse_s, \
       '95 \\% CI': CI}
df  = pd.DataFrame(data=d, index=param_names, columns = ['Unit', 'Median value', 'CV (\%)', 'r.s.e. (\%)', '95 \\% CI'])

df.to_latex('table.tex', float_format = '%.3g', escape = False)
