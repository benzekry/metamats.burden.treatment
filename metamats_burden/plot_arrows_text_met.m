function plot_arrows_text_met(AX, resection_time, max_time)
if ~exist('max_time', 'var')
    max_time = Inf;
end
font = 18;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arrows on top of the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
create_blank_space_top(1.2, gcf, AX)
set(AX, 'Units', 'normalized');
xA         = get(AX(1), 'Position');
xminAx     = xA(1);
xA2        = xA(2);
xA3        = xA(3);
xA4        = xA(4);
xmaxAx     = xminAx + xA3;
ymin_arrow = 0.90;
ymax_arrow = xA2 + xA4;
y_mid      = 1/2*(ymin_arrow + ymax_arrow);
annotation('arrow', [xminAx xminAx], [ymin_arrow ymax_arrow]);
if resection_time < max_time
    [figx_res_time, ~] = dsxy2figxy([resection_time, resection_time], [1, 1]);
    annotation('arrow', figx_res_time, [ymin_arrow ymax_arrow]);
    annotation('arrow', [xminAx figx_res_time(1)], [y_mid y_mid])
    annotation('arrow', [figx_res_time(1) xmaxAx], [y_mid y_mid]);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Text on top of arrows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ha_text = axes();
set(ha_text, 'Visible', 'off');
set(ha_text, 'Units', 'normalized');
set(ha_text, 'Position', [0, 0, 1, 1]);
text(xminAx, ymin_arrow*1.10, 'Orthotopic', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
text(xminAx, ymin_arrow*1.05, 'implantation', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
if resection_time < max_time
    text(figx_res_time(1), ymin_arrow*1.10, 'Surgery', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
%     text(figx_res_time(1), ymin_arrow*1.05, ['(t = ' num2str(resection_time) ')'], ...
%         'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
%         'Fontsize', font)
    x_mid_PT = 1/2*(xminAx + figx_res_time(1));
    text(x_mid_PT, y_mid*1.05, 'Pre-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    ht_PT = text(x_mid_PT, y_mid*0.98, 'PT', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font);
    x_mid_met = 1/2*(figx_res_time(1) + xmaxAx);
    text(x_mid_met, y_mid*1.05, 'Post-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    text(x_mid_met, y_mid*0.98, 'MB', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
end

