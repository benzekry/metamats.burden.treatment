% Methode des trapezes 1D pour un vecteur f de valeurs d'une fonction
% (subdivision homogene)
function int = quadrat_1D_trap(f,dt)
int = 1/2*dt*(f(1) + f(end)) + dt*sum(f(2:end-1));
end