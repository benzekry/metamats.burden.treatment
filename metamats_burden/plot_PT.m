if temps(end) < 500
    plot(ax1, temps(1:diagnosis_index), Vp(1:diagnosis_index))
    hold on
    plot(ax1, temps(diagnosis_index+1:end), Vp(diagnosis_index+1:end), '--')
    if k <= diagnosis_index
        plot(ax1, temps(k) - T1, Vp(k), 'or', 'MarkerFaceColor', 'r');
    else
        plot(ax1, temps(k) - T1, Vp(k), 'or')
    end
    hold off
    xlabel(ax1, 'Time (days)')
else
    plot(ax1, (temps(1:diagnosis_index) - T1)/30,...
        Vp(1:diagnosis_index))
    hold on
    plot(ax1, (temps(diagnosis_index+1:end) - T1)/30,...
        Vp(diagnosis_index+1:end), '--')
    if k <= diagnosis_index
        plot(ax1, (temps(k) - T1)/30, Vp(k), 'or', 'MarkerFaceColor', 'r');
    else
        plot(ax1, (temps(k) - T1)/30, Vp(k), 'or');
    end
    hold off
    xlabel(ax1, 'Time (months)')
end
set(ax1, 'xlim', [-T1/30, (temps(end) - T1)/30])
set(ax1, 'ylim', [1, 1e12], 'yscale', 'log')
line([0, 0], get(gca, 'ylim'),...
    'Linestyle', '--',...
    'Color', 'k')
ylabel(ax1, 'Size (cells)')
title(ax1, 'Primary tumor')
set_fonts_lines(ax1);