%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pops_PT, pops_met] = simulate_pop_PT_met_struct(...
        temps,...        
        model_struct,...
        fixed_effects,...
        psi,...             % covariance of the random effects
        param_transform,... 
        resection_time,...
        nb_seeds)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
temps_PT        = temps(temps <= resection_time);
temps_met       = temps(temps > resection_time);
pops_burden_PT  = zeros(nb_seeds, length(temps_PT) + length(temps_met));
pops_PT         = zeros(nb_seeds, length(temps_PT));
pops_met        = zeros(nb_seeds, length(temps_met));
rng(0) % for reproducibility
warning(['Caution. In the case of a lognormal distribution of the parameters,' ...
    ' and estimation performed by nlmefitsa, it is assumed that the covariance'...
    ' psi is the covariance of exp(param). This should be properly tested']);
for s = 1:nb_seeds
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Randomly generate parameters and deal with lognormal distribution
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    param_loc  = mvnrnd(fixed_effects, psi)';
    param_loc  = param_loc.*(1 - param_transform) + exp(param_loc).*param_transform;
    param_loc  = reshape(param_loc, 1, length(param_loc));    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Simulate model
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pops_burden_PT(s, :) = model_struct.model(param_loc, temps);    
    pops_PT(s, :)        = pops_burden_PT(s, 1:length(temps_PT));
    pops_met(s, :)       = pops_burden_PT(s,...
        (length(temps_PT) + 1):end);
end