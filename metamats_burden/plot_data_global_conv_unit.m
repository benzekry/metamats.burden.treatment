% Plots two time-dependent data sets on two axes. Typically made for
% primary tumor and metastases
function [AX, hPT, hMet] = plot_data_global_conv_unit(...
    index_postresection,...
    time_PT,...
    PT,...
    time_met,...
    met,...
    axisS,...
    conv_unit_PT,...
    conv_unit_met,...
    styleS)
if nargin >= 9
    style_PT  = styleS{1};
    style_met = styleS{2};
else    
    style_PT  = '*';
    style_met = 'o';
end
colors   = get(gca,'ColorOrder');
marker   = 10;
font     = 20;
fontaxes = 18;
line     = 1;
N        = length(PT);
for j = 1:N
   PT{j}  = conv_unit_PT(PT{j});
   met{j} = conv_unit_met(met{j});
end
figure(1)
clf
[AX, H1, H2] = plotyy(time_PT{1}, PT{1}, time_met{1}, met{1});
set(H1, 'LineStyle', 'none', 'Marker', 'none');
set(H2, 'LineStyle', 'none', 'Marker', 'none');
if nargin >= 5
    axis(AX(1), axisS{1});
    axis(AX(2), axisS{2});
else
    xLim1 = get(AX(1), 'Xlim');
    xLim2 = get(AX(2), 'xlim');
    set(AX(1), 'xlim', [0, xLim1(2)]);
    set(AX(2), 'xlim', [0, xLim2(2)]);
    yLim1 = get(AX(1), 'ylim');
    yLim2 = get(AX(2), 'ylim');
    set(AX(1), 'ylim', [0, yLim1(2)]);
    set(AX(2), 'ylim', [0, yLim2(2)]);
end
set(AX, {'ycolor'}, {'k';'k'});
set(AX, {'Xticklabel'}, {[];[]});
set(AX, {'Xtick'}, {[];[]});
set(AX, {'Yticklabel'}, {[];[]});
set(AX, {'Ytick'}, {[];[]});
hPlots = cell(1, N);
hold(AX(1), 'on')
hold(AX(2), 'on')
for i = 1:N
    figure(1)
    hPT = plot(AX(1),...
        time_PT{i}, PT{i},...
        style_PT,...
        'Color', colors(1,:),...
        'Linewidth', line,...
        'Markersize', marker);
    hMet = plot(AX(2), time_met{i}(index_postresection:end),...
        met{i}(index_postresection:end), style_met,...
        'Color', colors(2,:), 'Linewidth', line, 'Markersize', marker);            
    hPlots{i} = hPT;
end
figure(1)
set(AX(1), 'XtickMode', 'auto')
set(AX(1), 'XticklabelMode', 'auto')
set(AX, {'YtickMode'}, {'auto';'auto'}, {'YtickLabelMode'}, {'auto';'auto'})
set(AX(1), 'Fontsize', fontaxes)
set(AX(2), 'Fontsize', fontaxes)
xlabel(AX(1), 'Time (days)', 'Fontsize', font)
ylabel(AX(1), 'PT volume', 'Fontsize', font)
ylabel(AX(2), 'Met burden', 'Fontsize', font)
hold(AX(1), 'off');
hold(AX(2), 'off');
end