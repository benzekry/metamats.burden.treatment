% Computes the metastatic mass via a first order quadrature
% Growth PT follows model growth_model_PT (with parameters param_PT) 
% Growth of Mets follows model growth_model_met (with parameter set param)
% flagNb = 0 or 1 depending on whether we want the number of mets to be
% computed or not
% Model is designed to be plugged to nlmefitsa, hence time is divided into
% timePT and timeMet
function [burden_PT, nb] = model_burden_nb_PT_old(...
    param,...            metastases parameters [mu, growth_parameters]
    time,...             time discretization at which computation is made
    growth_model_PT,...  growth model of the primary tumor
    growth_model_met,... growth model of the metastases
    param_PT,...         primary tumor growth parameters
    flag_nb)             % 1 = compute number, 0 = don't compute
    global V0 Vinj gamma resection_time % should be moved to arguments in a future version
    dt       = 0.1;          % time discretization step    
    time_PT  = time(time <= resection_time);
    warining('This will not work if there are time indices inferior to resection_time in metastatic measurements')
    time_met = time(time > resection_time);
    mu       = param(1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Time discretization for FFT
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    N        = 2^nextpow2(max(time)/dt);  % number of discretization steps optimised for FFT
    T        = N*dt;
    t_vec    = dt:dt:T;                   % calculation times       
    loc_PT   = int32(time_PT/dt);         % indices corresponding to time_PT on the t_vec grid
    loc_met  = int32(time_met/dt);        % indices corresponding to time_met on the t_vec grid
    loc      = int32(time/dt);            % indices corresponding to time on the t_vec grid
    if any(abs(time/dt - double(loc)) > eps('single'))
        error('Time step too large: Observation times are not on the grid')
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Size of the PT and the "generic" metastasis
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Vp      = growth_model_PT(param_PT, t_vec - dt, Vinj); % calculate Vp = X(t, V0, 0) for convolution calculation
    PT      = Vp(loc_PT + 1);
    X       = growth_model_met(param(2:end), t_vec, V0); % calculate X(t;V0,0) for convolution calculation    
    beta_xp = mu*Vp.^gamma.*((t_vec - dt) < resection_time);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculation of the number
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    nb      = zeros(1,N);
    if flag_nb == 1
        nb(1) = 0;
        for n = 2:N
            nb(n) = quadrat_1D_trap(beta_xp(1:n),dt);
        end
    end
    nb      = nb(loc_met + 1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculation of the metastatic burden (on 0:dt:T)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    M         = [0 euler_exp_FFT(X, beta_xp, N, T)];   % FFT-based explicit Euler quadrature            
    burden    = M(loc_met + 1);    
    burden_PT = [PT, burden]';
    if any(~isfinite(burden_PT))
        warning('model_nb_burden_PT returns Inf value, model set to 0')
        burden_PT = zeros(1, length(burden_PT));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function u = euler_exp_FFT(f, g, N, T)    
    f_ext = [f zeros(1,N)];
    g_ext = [g zeros(1,N)];   
    Ff    = fft(f_ext);
    Fg    = fft(g_ext);    
    u_ext = (T/N)*ifft(Ff.*Fg);
    u     = u_ext(1:N);
end