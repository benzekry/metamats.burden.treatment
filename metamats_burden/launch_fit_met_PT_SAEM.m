%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and build models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
build_models_met_PT;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
folder_glob   = 'fit_SAEM';
data_PT_name  = 'PT';
data_met_name = 'met';
models        = {SameGrowth};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist(folder_glob, 'dir')
    mkdir(folder_glob);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Format the data for nlmefitsa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_PT  = evalin('base', data_PT_name);
time_PT  = evalin('base', ['time_' data_PT_name]);
data_met = evalin('base', data_met_name);
time_met = evalin('base', ['time_' data_met_name]);
N        = length(data_met);
V        = [];
X        = [];
Y        = [];
GROUP    = [];
for j = 1:N
    data_met_loc = data_met{j};
    time_met_loc = time_met{j};
    time_PT_loc  = time_PT{j};
    data_PT_loc  = data_PT{j};
    n_PT         = length(data_PT_loc);
    n_met        = length(data_met_loc);    
    X            = [X; time_PT_loc'; time_met_loc']; % column time vector
    Y            = [Y; data_PT_loc'; data_met_loc']; % column data vector
    GROUP        = [GROUP; j*ones(n_PT + n_met, 1)]; % GROUP denotes here animal index    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fit the models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
L = length(models);
for l = 1:L % loop over possibly multiple models
    model           = models{l}.model;
    P               = length(models{l}.param0);
    param_transform = ones(P, 1); % 1 = lognormal, 0 = normal
    param0          = models{l}.param0'.*(1 - param_transform) + log(models{l}.param0)'.*param_transform;    
    folder          = [folder_glob '/' data_met_name '/' models{l}.folder];
    if ~exist(folder, 'dir')
        mkdir(folder);
    end
    display(folder)    
    [beta, psi, stats, B] = nlmefitsa(...
        X,...
        Y,...
        GROUP,...
        V,...
        model,....
        param0,...
        'CovPattern', ones(P,P),...
        'Errormodel', 'proportional',...
        'ComputeStdErrors', true,...
        'LogLikMethod', 'lin',...
        'ParamTransform', param_transform);
    save([folder '/fit.mat'])
end
