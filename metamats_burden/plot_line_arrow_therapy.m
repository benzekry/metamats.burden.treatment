function plot_line_arrow_therapy(ax, t_start, t_end, plot_line_t_start)
font  = 18;
axes(ax)
y_lim = get(ax, 'YLim');
if exist('t_start', 'var') && ~isempty(t_start)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Lines
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if exist('plot_line_t_start', 'var') && plot_line_t_start == false        
    else
        hl = line([t_start, t_start], y_lim);        
        set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
    end
    hl = line([t_end, t_end], y_lim);
    set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Arrow
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    t_f          = t_end + 0.5;
    [figx_tx, ~] = dsxy2figxy([t_start t_end], [1  1]);
    annotation('doublearrow', figx_tx, [0.65 0.65]);
    text(t_start + (t_f-t_start)/2 -1, 3.4481e+07, 'Tx',...
        'VerticalAlignment', 'bottom',...
        'Fontsize', font,...
        'Color', 'k');
end