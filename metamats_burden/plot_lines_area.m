function plot_lines_area(AX, resection_time)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dashed line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear line
% axes(AX);
y_lim = get(AX(1), 'YLim');
hl    = line([resection_time, resection_time], y_lim);
set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grey area
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX, 'on');
x_lim = get(AX, 'XLim');
har   = area([x_lim(1), resection_time], [y_lim(2), y_lim(2)], 'Parent', AX);
set(har, 'FaceColor', [192, 192, 192]/255, 'LineStyle', 'none');
uistack(har, 'bottom')
set(AX, 'Layer', 'top');
hold(AX, 'off');
