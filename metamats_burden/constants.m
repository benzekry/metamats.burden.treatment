global Vinj V0 gamma
Vinj         = 1; % (mm^3)
lambda_iv    = 0.837; %(day^-1) based on in vitro fit to the average
DT_invitro   = log(2)/lambda_iv*24; % (hours)
convPhotCell = 10; % 1 cell \simeq convPhotCEll photons/seconds
convPhotMm   = convPhotCell*1e6; % 1 mm3 \simeq convPhotMm photons/seconds
phot2cell    = @(x) x./convPhotCell;
cell2phot    = @(x) x*convPhotCell;
mm2cell      = @(x) x*1e6;
cell2mm      = @(x) x*1e-6;
phot2mm      = @(x) cell2mm(phot2cell(x));
mm2phot      = @(x) cell2phot(mm2cell(x));
V0           = cell2phot(1); % (photons/second)
gamma        = 1; % power in the dissemination rate