figure(1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot PT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(1), 'on')
hPTModel50 = plot(AX(1), temps_PT, PopPT50, 'b', 'Linewidth', 2);
hPTModel10 = plot(AX(1), temps_PT, PopPT10, '--b', 'Linewidth', 2);
hPTModel90 = plot(AX(1), temps_PT, PopPT90, '--b', 'Linewidth', 2);
hold(AX(1), 'off')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot Met
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(2), 'on')
hMetModel50 = plot(AX(2), temps_met, PopMet50, 'r', 'Linewidth', 2);
hMetModel10 = plot(AX(2), temps_met, PopMet10, '--r', 'Linewidth', 2);
hMetModel90 = plot(AX(2), temps_met, PopMet90, '--r', 'Linewidth', 2);
hold(AX(2), 'off')
hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Axis labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ylabel(AX(1), 'Primary tumor size (cells)')
ylabel(AX(2), 'Metastatic burden (cells)')
hf = gcf;
max_size = 1e10;
min_size_noLog = 0;
max_size_noLog = 1e9;
set(AX(1), 'Ylim', [min_size_noLog, max_size_noLog])
set(AX(2), 'Ylim', [min_size_noLog, max_size_noLog])
print_Seb(format, file)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Log scale
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(AX(1), 'Yscale', 'log', 'Ylim', [min_size, max_size])
set(AX(2), 'Yscale', 'log', 'Ylim', [min_size, max_size])
vis_state = get(AX(2), 'Visible');
if isequal(vis_state, 'on')
    set(AX(1), 'Units', 'pixels');
    set(AX(2), 'Units', 'pixels');
    hf = gcf;
    position = get(hf, 'Position');
    position(3) = position(3)*1.05;
    set(hf, 'Position', position);
%     hf.Position(3) = hf.Position(3)*1.05;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Arrows on top of the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
create_blank_space_top(1.2, gcf, AX)
set(AX(1), 'Units', 'normalized');
set(AX(2), 'Units', 'normalized');
xA = get(AX(1), 'Position');
xminAx = xA(1);
% xminAx = AX(1).Position(1);
xA2 = xA(2);
xA3 = xA(3);
xA4 = xA(4);
% xmaxAx = AX(1).Position(1)+AX(1).Position(3);
xmaxAx = xminAx + xA3;
ymin_arrow = 0.90;
% ymax_arrow = AX(1).Position(2)+AX(1).Position(4);
ymax_arrow = xA2 + xA4;
y_mid = 1/2*(ymin_arrow+ymax_arrow);
annotation('arrow', [xminAx xminAx], [ymin_arrow ymax_arrow]);
if resection_time < max_time
    [figx_resTime, ~] = dsxy2figxy([resection_time, resection_time], [1, 1]);
    annotation('arrow', figx_resTime, [ymin_arrow ymax_arrow]);
    annotation('arrow', [xminAx figx_resTime(1)], [y_mid y_mid])
    annotation('arrow', [figx_resTime(1) xmaxAx], [y_mid y_mid]);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Middle dashed line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear line
y_lim = get(AX(1), 'YLim');
% hl = line([resection_time, resection_time], AX(1).YLim);
hl = line([resection_time, resection_time], y_lim);
set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grey area
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(1), 'on');
x_lim = get(AX(1), 'XLim');
% har = area([AX(1).XLim(1), resection_time], [AX(1).YLim(2), AX(1).YLim(2)], 'Parent', AX(1));
har = area([x_lim(1), resection_time], [y_lim(2), y_lim(2)], 'Parent', AX(1));
set(har, 'FaceColor', [192, 192, 192]/255, 'LineStyle', 'none');
uistack(har, 'bottom')
% AX(1).Layer = 'top';
set(AX(1), 'Layer', 'top');
% layer = get(AX(1), 'Layer');
% layer = 'top';
hold(AX(1), 'off');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Text on top of arrows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ha_text = axes();
set(ha_text, 'Visible', 'off');
set(ha_text, 'Units', 'normalized');
set(ha_text, 'Position', [0, 0, 1, 1]);
% ha_text.Visible = 'off';
% ha_text.Units = 'normalized';
% ha_text.Position = [0, 0, 1, 1];
text(xminAx, ymin_arrow*1.10, 'Orthotopic', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
text(xminAx, ymin_arrow*1.05, 'implantation', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
if resection_time < max_time
    text(figx_resTime(1), ymin_arrow*1.10, 'Surgery', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    text(figx_resTime(1), ymin_arrow*1.05, ['(t = ' num2str(resection_time) ')'], ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    x_midPT = 1/2*(xminAx+figx_resTime(1));
    text(x_midPT, y_mid*1.05, 'Pre-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    ht_PT = text(x_midPT, y_mid*0.98, 'PT', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font);
    x_midMet = 1/2*(figx_resTime(1)+xmaxAx);
    text(x_midMet, y_mid*1.05, 'Post-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    text(x_midMet, y_mid*0.98, 'MB', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print figure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% print_Seb(format, [file '_log'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Legend
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% handleLines = [hDataPT, ...    
%     hPTModel50, hPTModel10, ...    
%     hDataMet, ...h_medianData, h_MetData10...
%     hMetModel50, hMetModel10];
% Legende = {'Data PT', ...    
%     'Median model PT', 'Model prct PT', ...
%     'Data Met', ...'Empirical median Met', 'Empirical prct Met', ...
%     'Model median Met', 'Model prct Met'};
% hLeg = formatLegendOutsideDoublePlot(Legende, handleLines, AX);
% set(hLeg, 'Box', 'off');
% PaperPosition = get(gcf, 'PaperPosition');
% set(gcf, 'PaperSize', [PaperPosition(3), PaperPosition(4)]);
% set(gcf, 'PaperPosition', [0, 0, PaperPosition(3), PaperPosition(4)]);
% % print(format, [dossierGlob '/' dataNameMet '/' modelStructMet.folder '/PopFit_cell'])
% print(format, [dossierGlob '/' dataNameMet '/' modelStructMet.folder '/PopFit_log_cell'])