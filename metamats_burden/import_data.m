global resection_time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time_PT_vect    = xlsread('data.xlsx', 'C3:J3');
PT_mat          = xlsread('data.xlsx', 'C4:J9');
N               = size(PT_mat, 1);
PT              = cell(1, N);
time_PT         = cell(1, N);
resection_times = zeros(N, 1);
for i=1:N
    PT{i}              = PT_mat(i, :);
    time_PT{i}         = time_PT_vect;
    resection_times(i) = time_PT_vect(end);
end   
resection_time = resection_times(1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time_met_vect = xlsread('data.xlsx', 'K3:R3');
met_mat       = xlsread('data.xlsx', 'K4:R9');
met           = cell(1, N);
time_met      = cell(1, N);
for i = 1:N
    met{i}      = met_mat(i, (met_mat(i, :) > 0) & (time_met_vect > resection_time)); % only keep values of metastatic burden posterior to resection time. If not, function model_burden_nb_PT is not working properly
    time_met{i} = time_met_vect((met_mat(i, :) > 0) & (time_met_vect > resection_time));
end  