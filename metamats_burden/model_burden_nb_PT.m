% Computes the metastatic mass via a first order quadrature
% Growth PT follows model growth_model_PT (with parameters param_PT) 
% Growth of Mets follows model growth_model_met (with parameter set param)
% flagNb = 0 or 1 depending on whether we want the number of mets to be
% computed or not
% Model is designed to be plugged to nlmefitsa, hence time is divided into
% time_PT and time_Met
function [burden_PT, nb] = model_burden_nb_PT(...
    param,...            metastases parameters [mu, growth_parameters]
    time,...             time discretization at which computation is made
    growth_model_PT,...  growth model of the primary tumor
    growth_model_met,... growth model of the metastases
    param_PT,...         primary tumor growth parameters
    flag_nb)             % 1 = compute number, 0 = don't compute
    global Vinj % should be moved to arguments in a future version
    resection_time = param_PT(end);    
    time_PT        = time(time <= resection_time);
    time_met       = time(time > resection_time);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Size of the PT and the "generic" metastasis
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    PT            = growth_model_PT(param_PT(1:2), time_PT, Vinj); 
    if isempty(time_met)
        burden = [];
    else
        [burden, nb]  = model_burden_nb(...
            param,...
            time_met,...
            growth_model_PT,...
            growth_model_met,...
            param_PT,...
            flag_nb);        
    end
    if size(PT, 2) > size(PT, 1)
        burden_PT     = [PT, burden];
    else
        burden_PT     = [PT; burden];
    end
    if any(~isfinite(burden_PT))
        warning('model_nb_burden_PT returns Inf value, model set to 0')
        burden_PT = zeros(1, length(burden_PT));
    end
end