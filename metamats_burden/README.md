# METAMATS_BURDEN

This Matlab software is a minimal code for fitting simultaneously primary tumor growth and metastatic burden data using nonlinear mixed-effects modeling [1].

### Purpose

* Fit simultaneously data of primary tumor (PT) growth and metastatic burden (MB)
* Uses a deterministic model without secondary dissemination based on a convolution formula for the metastatic burden
* Nonlinear mixed-effects modeling (NLME, using Matlab function nlmefitsa, SAEM algorithm)
* Allows different units for PT and MB
* Plots the visual predictive check based on simulation of the population recovered from the NLME fit

### How to use it

* Dependencies: ``carcinom``, `` metamats_core ``
* Data provided by an excel file
* Loaded in `` import_data``
* Models defined in ``build_models_met_PT``
* Fit launched in `` launch_fit_met_PT_SAEM ``
* Simulation plotted in `` manips_plot_fit_met_PT_SAEM ``


[1] Benzekry, S., Tracz, A., Mastri, M., Corbelli, R., Barbolosi, D., & Ebos, J. M. L. (2016). Modeling Spontaneous Metastasis following Surgery: An In Vivo-In Silico Approach. Cancer Research, 76(3), 535–547. http://doi.org/10.1158/0008-5472.CAN-15-1389
