global Vinj
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and build models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
build_models_met_PT;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_name_PT        = 'PT';
data_name_met       = 'met';
model_struct        = SameGrowth;
folder              = 'fit_SAEM/met/same_growth';
conv_unit_PT        = mm2cell;
conv_unit_met       = phot2cell;
% flag that decides whether to use parameters from a monolix fit (1) or
% manually fixed (0)
flag_param_fit      = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model for PT and mets growth 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model_met_f         = @(param, time, param_PT) model_burden_nb(... % Same growth
    [param(1),  param_PT],...
    time,...
    @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2mm(1), X0), ...
    @(param, time, X0) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2phot(1), X0),...
    param_PT,...
    0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Model for PT growth 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model_PT_f          = @(param, temps) gompertz_exp_alpha0_beta(param, DT_invitro, temps, cell2mm(1), Vinj);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_param_fit == 1
    fit_r               = load([folder '/fit.mat']);
    fixed_effects_PT    = fit_r.beta(2:3); % should be changed if the model is changed
    covariance_PT       = fit_r.psi(2:3, 2:3);
    param_transform_PT  = fit_r.param_transform(2:3);
    fixed_effects_met   = fit_r.beta;
    covariance_met      = fit_r.psi;
    param_transform_met = fit_r.param_transform;
else    
    fixed_effects_PT    = log([2; 0.1]); %log([1.9; 0.0893]);
    covariance_PT       = diag([0, 0]);
    fixed_effects_met   = log(SameGrowth.param0) %log([1e-5; 2; 0.1]); %log([10^6*4.43e-11; 1.9; 0.0893]);
    covariance_met      = diag([0, 0, 0]);
    param_transform_PT  = ones(2, 1);
    param_transform_met = ones(3, 1);
    folder              = [folder '/mano'];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
max_time        = 77;
temps_PT        = 0:0.1:resection_time;
temps_met       = 0:0.1:max_time;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_set_PT         = evalin('base', data_name_PT);
time_PT             = evalin('base', ['time_' data_name_PT]);
data_set_met        = evalin('base', data_name_met);
time_met            = evalin('base', ['time_' data_name_met]);
index_postresection = 1; %First time index post-resection (retains only the metastatic burden post-resection)
max_time            = 77;
max_size            = 4e9;
min_size            = 1e3;
[AX, hDataPT, hDataMet] = plot_data_global_conv_unit(...
    index_postresection,...
    time_PT,...
    data_set_PT,...
    time_met, data_set_met, ...
    {[0, max_time, min_size, max_size], [0, max_time, min_size, max_size]},...
    conv_unit_PT,...
    conv_unit_met);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate population
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nb_simu             = 1000;
[pops_PT, pops_met] = simulate_pop_PT_met(...
        temps_PT,...
        temps_met,...
        model_PT_f,...
        model_met_f,...
        fixed_effects_PT,...    % fixed effects of the PT parameters
        covariance_PT,...       % covariance of the random effects for the PT parameters
        param_transform_PT,... 
        fixed_effects_met,...
        covariance_met,...            % covariance of the random effects for the met parameters
        param_transform_met,...
        resection_time,...
        nb_simu);                        % number of simulations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot model simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pops_PT                    = conv_unit_PT(pops_PT);
pops_met                   = conv_unit_met(pops_met);
flag_plot_met_distribution = 1;
file                       = [folder '/pop_plot']; % path to the output file
plot_PT_met_model(...
        AX,...
        temps_PT,...
        temps_met,...
        pops_PT,...
        pops_met,...
        resection_time,...
        min_size,...
        max_size,...
        max_time,...
        flag_plot_met_distribution,...
        [],...
        file);