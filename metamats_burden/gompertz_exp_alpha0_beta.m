function V = gompertz_exp_alpha0_beta(param, DT_invitro, temps, Vc, V0)
% param  =  [alpha_0, beta] parameters of Gompertz growth,  according to
% dV/dt  =  (alpha_0-beta*log(V/Vc))*V
% hence alpha_  =  relative growth rate at V = 1 cell (volume Vc)
% DTinvitro  =  doubling time in vitro in hours
DT_invitro = DT_invitro/24; % conversion in days
lambda    = log(2)/DT_invitro;
DTs       = doubling_time_gompertz(param, temps, Vc, V0);
if isempty(DTs > DT_invitro)
    error('The gompertzExp model is not valid as is. Gompertz doubling times cannot be computed because the ratio V0exp(lambda*t)/K is always <1/2');
end
ind_exp    = find((DTs < DT_invitro).*(DTs>0));
temps_exp  = temps(ind_exp);
temps_gomp = temps;temps_gomp(ind_exp) = [];
Vexp       = V0*exp(lambda*temps_exp);
if isempty(temps_exp)
    tIgomp = 0;
    VIgomp = V0;
else
    tIgomp = temps_exp(end);
    VIgomp = Vexp(end);
end
Vgomp = gompertztIVI(param, temps_gomp, tIgomp, VIgomp);
if size(temps, 2) > size(temps, 1)
    V     = [Vexp, Vgomp];
else
    V     = [Vexp; Vgomp];
end
    function V = gompertztIVI(param,  temps,  tI,  VI)
        V = gompertzV0_alpha0beta(param,  temps-tI,  Vc,  VI);
    end
end
