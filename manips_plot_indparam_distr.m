clear all
close all
clc

import_data

folder_fit     = './fit_PT_met_SAEM';
fit_r          = load([folder_fit '/fit.mat']);
beta           = fit_r.beta;
psi            = fit_r.psi;
[param_inds, param_ind_d]  = compute_param_inds(...
    folder_fit,...
    subgroups,...
    [2,7:11],...
    subgroup_names,...
    0);
param_labels                = {'mu', 'alpha', 'beta', 'k'};
param_names                 = {'mu', 'alpha', 'beta', 'k'};


shrinkage = compute_shrinkage(...
    folder_fit,...
    subgroups,...
    [2,7:11]);


colors       = get(gca,'ColorOrder');

%% mu
h = histogram(param_inds(:,1), 30);

% x is the probability/height for each bin
x = h.Values/sum(h.Values*h.BinWidth);

% n is the center of each bin
n = h.BinEdges + (0.5*h.BinWidth);
n(end) = [];

bar(n,x, 'FaceColor', colors(1,:,:));

hold on
x2 = 0:0.00001:1.5e-3;
f2 = lognpdf(x2,beta(1),sqrt(psi(1,1)));
plot(x2,f2, 'Color', colors(2,:,:));
xlim([-0.00002, 1.5e-3])
xlabel('\mu')
ylabel('density')
dim = [.62 .6 .3 .3];
str = ['Shrinkage = ', num2str(shrinkage(1),3), '%' ];
t = annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w');
t.FontSize = 18;
set_fonts_lines(gca)
export_fig('./diagnostic_plots/distr_ind_param/mu.pdf')
close all

%% alpha
h = histogram(param_inds(:,2));

% x is the probability/height for each bin
x = h.Values/sum(h.Values*h.BinWidth);

% n is the center of each bin
n = h.BinEdges + (0.5*h.BinWidth);
n(end) = [];

bar(n,x, 'FaceColor', colors(1,:,:));
hold on
x2 = 0.4:0.001:3.5;
f2 = lognpdf(x2,beta(2),sqrt(psi(2,2)));
plot(x2,f2, 'Color', colors(2,:,:));
xlabel('\alpha')
ylabel('density')
dim = [.62 .6 .3 .3];
str = ['Shrinkage = ', num2str(shrinkage(2),3), '%' ];
t = annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w');
t.FontSize = 18;
legend('Empirical distribution', 'Theoretical distribution','Location','northwest')
set_fonts_lines(gca)
export_fig('./diagnostic_plots/distr_ind_param/alpha.pdf')
close all

%% beta
h = histogram(param_inds(:,3));

% x is the probability/height for each bin
x = h.Values/sum(h.Values*h.BinWidth);

% n is the center of each bin
n = h.BinEdges + (0.5*h.BinWidth);
n(end) = [];

bar(n,x, 'FaceColor', colors(1,:,:));
hold on
x2 = 0.03:0.001:0.16;
f2 = lognpdf(x2,beta(3),sqrt(psi(3,3)));
plot(x2,f2, 'Color', colors(2,:,:));
xlabel('\beta')
ylabel('density')
dim = [.62 .6 .3 .3];
str = ['Shrinkage = ', num2str(shrinkage(3),3), '%' ];
t = annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w');
t.FontSize = 18;
set_fonts_lines(gca)
export_fig('./diagnostic_plots/distr_ind_param/beta.pdf')
close all


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % use for k only
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear all
% close all
% clc
% 
% import_data
% 
% folder_fit     = './fit_PT_met_SAEM';
% fit_r          = load([folder_fit '/fit.mat']);
% beta           = fit_r.beta;
% psi            = fit_r.psi;
% [param_inds, param_ind_d]  = compute_param_inds(...
%     folder_fit,...
%     subgroups,...
%     [7:11],...
%     subgroup_names,...
%     0);
% 
% shrinkage = compute_shrinkage(...
%     folder_fit,...
%     subgroups,...
%     [7:11]);
% 
% 
% colors       = get(gca,'ColorOrder');
% 
% %% k
% h = histogram(param_inds(:,4));
% 
% % x is the probability/height for each bin
% x = h.Values/sum(h.Values*h.BinWidth);
% 
% % n is the center of each bin
% n = h.BinEdges + (0.5*h.BinWidth);
% n(end) = [];
% 
% bar(n,x, 'FaceColor', colors(1,:,:));
% hold on
% x2 = 0:0.001:0.08;
% f2 = lognpdf(x2,beta(4),sqrt(psi(4,4)));
% plot(x2,f2,'Color', colors(2,:,:));
% xlabel('k')
% ylabel('density')
% dim = [.62 .6 .3 .3];
% str = ['Shrinkage = ', num2str(shrinkage(4),3), '%' ];
% t = annotation('textbox',dim,'String',str,'FitBoxToText','on', 'Edgecolor', 'w');
% t.FontSize = 18;
% set_fonts_lines(gca)
% export_fig('./diagnostic_plots/distr_ind_param/k.pdf')