clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load data and build models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
build_models_met_PT;
i_remove = [1:6, 34:55, 133:170];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
folder        = './fit_PT_met_SAEM';
data_PT_name  = 'PT';
data_met_name = 'met';
model_v       = Model_vehicle1.model;
model_t       = Model_ther1.model;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist(folder, 'dir')
    mkdir(folder);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Format the data for nlmefitsa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_PT  = evalin('base', data_PT_name);
data_PT(:,i_remove) = [];
time_PT  = evalin('base', ['time_' data_PT_name]);
time_PT(:,i_remove) = [];
data_met = evalin('base', data_met_name);
data_met(:,i_remove) = [];
time_met = evalin('base', ['time_' data_met_name]);
time_met(:,i_remove) = [];
N        = length(data_met);
g        = length(subgroups);
V        = zeros(N,1);
for k = 1:g
    V(subgroups{k}) = k;
end
V(i_remove) = [];
X        = [];
Y        = [];
GROUP    = [];
for j = 1:N
    data_met_loc = data_met{j};
    time_met_loc = time_met{j};
    time_PT_loc  = time_PT{j};
    data_PT_loc  = data_PT{j};
    n_PT         = length(data_PT_loc);
    n_met        = length(data_met_loc);    
    X            = [X; time_PT_loc'; time_met_loc']; % column time vector
    Y            = [Y; data_PT_loc'; data_met_loc']; % column data vector
    GROUP        = [GROUP; j*ones(n_PT + n_met, 1)]; % GROUP denotes here animal index 
end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Fit the models
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model           = @(param,time,V) model_all_Su_log(param, time, V, model_v, model_t);
P               = 4;
param_transform = ones(P, 1); % 1 = lognormal, 0 = normal
param0          = [4e-11, 1.9, 0.09, 0.037];
param0          = param0'.*(1 - param_transform) + log(param0)'.*param_transform;

display(folder)
[beta, psi, stats, B] = nlmefitsa(...
    X,...
    log(Y),...
    GROUP,...
    V,...
    model,....
    param0,...
    'CovPattern', ones(P,P),...
    'Errormodel', 'proportional',...
    'ComputeStdErrors', true,...
    'LogLikMethod', 'lin',...
    'ParamTransform', param_transform);
save([folder '/fit.mat'])