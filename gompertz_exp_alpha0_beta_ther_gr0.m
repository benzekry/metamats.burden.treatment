function V = gompertz_exp_alpha0_beta_ther_gr0(param, DT_invitro, temps, Vc, tI, VI, t_start, t_end)
% param  =  [alpha_0, beta] parameters of Gompertz growth,  according to
% dV/dt  =  (alpha_0-beta*log(V/Vc))*V
% hence alpha_  =  relative growth rate at V = 1 cell (volume Vc)
% DTinvitro  =  doubling time in vitro in hours
temps_1 = temps(temps <= t_start);
temps_2 = temps((t_start < temps)&(temps <= t_end));
temps_3 = temps(t_end < temps);
V_1     = gompertz_exp_alpha0_beta(param, DT_invitro, temps_1 - tI, Vc, VI);
if ~isempty(V_1)
    V_2     = V_1(end)*ones(size(temps_2));
else
    V_2     = VI*ones(size(temps_2));
end
if ~isempty(V_2)
    V_3     = gompertz_exp_alpha0_beta(param, DT_invitro, temps_3 - temps_2(end), Vc, V_2(end));
else
    V_3     = gompertz_exp_alpha0_beta(param, DT_invitro, temps_3 - tI, Vc, VI);
end
V_1 = reshape(V_1, length(V_1),1);
V_2 = reshape(V_2, length(V_2),1);
V_3 = reshape(V_3, length(V_3),1);
V = [V_1; V_2; V_3]';
end


