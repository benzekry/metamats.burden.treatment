function [param_inds, param_ind_d] = compute_param_inds(...
    folder_fit,...
    subgroups,...
    idx_subgroups,...
    subgroup_names,...
    flag_log_mu)
fit_r        = load([folder_fit '/fit.mat']);
nb_animals   = 0;
for idx_subgroup = 1:length(subgroups)
    nb_animals = nb_animals + length(subgroups(idx_subgroup));
end
param_inds   = zeros(nb_animals, length(fit_r.beta));
comp        = 0;
param_ind_d = containers.Map;
s           = 1;
for g = idx_subgroups    
    param_inds_group = zeros(length(subgroups{g}), 4);
    comp_group       = 0; 
    for j = s:s+ length(subgroups{g}) -1        
        comp           = comp + 1;
        comp_group     = comp_group + 1;
        param_ind      = exp(fit_r.beta + fit_r.B(:, j));
        if flag_log_mu == 1
            param_ind(1)   = log(param_ind(1));
        end
        param_inds_group(comp_group,:) = param_ind'; % solo group
        param_inds(comp, :)            = param_ind'; % tutti 
    end
    s = j+1;
    param_ind_d(subgroup_names{g}) = param_inds_group;
end

