function plot_PT_met_model_ind(...
    AX,...
    temps_PT,...
    temps_met,...
    PT_size,...
    met_burden,...
    resection_time,...
    min_size,...
    max_size,...
    max_time,...
    flag_plot_met_distrib,...
    t_ther,...
    file)
font     = 18;
if ~exist('max_size', 'var')
    max_size = 1e10;
end
colors   = get(gca,'ColorOrder');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot PT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(1), 'on')
if ~isnan(t_ther(1))
plot([t_ther(1), t_ther(1)], get(AX(1), 'YLim'), '--k')
plot([t_ther(2), t_ther(2)], get(AX(1), 'YLim'), '--k')
end
h_PT = plot(AX(1), temps_PT, PT_size,...
    'Color', colors(1, :), 'Linewidth', 2);
hold(AX(1), 'off')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot Met
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_plot_met_distrib == 1
    hold(AX(2), 'on')
    h_met = plot(AX(2), temps_met,met_burden,...
        'Color', colors(2, :), 'Linewidth', 2);
    hold(AX(2), 'off')
    hold off
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Axis labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ylabel(AX(1), 'Primary tumor size (cells)')
ylabel(AX(2), 'Metastatic burden (cells)')
min_size_noLog = 0;
max_size_noLog = 1e9;
set(AX(1), 'Ylim', [min_size_noLog, max_size_noLog])
set(AX(2), 'Ylim', [min_size_noLog, max_size_noLog])
export_fig([file 'linscale.pdf'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Log scale
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(AX(1), 'Yscale', 'log', 'Ylim', [min_size, max_size])
set(AX(2), 'Yscale', 'log', 'Ylim', [min_size, max_size])
vis_state = get(AX(2), 'Visible');
if isequal(vis_state, 'on')
    set(AX(1), 'Units', 'pixels');
    set(AX(2), 'Units', 'pixels');
    hf = gcf;
    position = get(hf, 'Position');
    position(3) = position(3)*1.05;
    set(hf, 'Position', position);
%     hf.Position(3) = hf.Position(3)*1.05;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arrows on top of the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
create_blank_space_top(1.2, gcf, AX)
set(AX(1), 'Units', 'normalized');
set(AX(2), 'Units', 'normalized');
xA         = get(AX(1), 'Position');
xminAx     = xA(1);
xA2        = xA(2);
xA3        = xA(3);
xA4        = xA(4);
xmaxAx     = xminAx + xA3;
ymin_arrow = 0.90;
ymax_arrow = xA2 + xA4;
y_mid      = 1/2*(ymin_arrow+ymax_arrow);
annotation('arrow', [xminAx xminAx], [ymin_arrow ymax_arrow]);
if resection_time < max_time
    [figx_res_time, ~] = dsxy2figxy([resection_time, resection_time], [1, 1]);
    annotation('arrow', figx_res_time, [ymin_arrow ymax_arrow]);
    annotation('arrow', [xminAx figx_res_time(1)], [y_mid y_mid])
    annotation('arrow', [figx_res_time(1) xmaxAx], [y_mid y_mid]);
    if exist('t_start', 'var') && ~isempty(t_start)
        [figx_tx_time, ~] = dsxy2figxy([t_start, t_start], [1, 1]);
        annotation('arrow', figx_tx_time, [ymin_arrow ymax_arrow]);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Middle dashed line
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear line
y_lim = get(AX(1), 'YLim');
hl    = line([resection_time, resection_time], y_lim);
set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
if exist('t_start', 'var') && ~isempty(t_start)
    hl = line([t_start, t_start], y_lim);
    set(hl, 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grey area
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(1), 'on');
x_lim = get(AX(1), 'XLim');
har   = area([x_lim(1), resection_time], [y_lim(2), y_lim(2)], 'Parent', AX(1));
set(har, 'FaceColor', [192, 192, 192]/255, 'LineStyle', 'none');
uistack(har, 'bottom')
set(AX(1), 'Layer', 'top');
hold(AX(1), 'off');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Text on top of arrows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ha_text = axes();
set(ha_text, 'Visible', 'off');
set(ha_text, 'Units', 'normalized');
set(ha_text, 'Position', [0, 0, 1, 1]);
text(xminAx, ymin_arrow*1.10, 'Orthotopic', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
text(xminAx, ymin_arrow*1.05, 'implantation', ...
    'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
    'Fontsize', font);
if resection_time < max_time
    text(figx_res_time(1), ymin_arrow*1.10, 'Surgery', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    text(figx_res_time(1), ymin_arrow*1.05, ['(t = ' num2str(resection_time) ')'], ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    x_mid_PT = 1/2*(xminAx+figx_res_time(1));
    text(x_mid_PT, y_mid*1.05, 'Pre-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    ht_PT = text(x_mid_PT, y_mid*0.98, 'PT', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font);
    x_mid_met = 1/2*(figx_res_time(1)+xmaxAx);
    text(x_mid_met, y_mid*1.05, 'Post-surgical', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
    text(x_mid_met, y_mid*0.98, 'MB', ...
        'VerticalAlignment', 'top', 'HorizontalAlignment', 'center', ...
        'Fontsize', font)
end
export_fig([file '.pdf'])