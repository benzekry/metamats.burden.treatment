%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script simulates the effect of different doses and duration of neoadjuvant treatment
% on primary and secondary tumor kinetics
% It generates the plots of Figure 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath("code/carcinom"))
addpath(genpath("code/metamats_burden"))
output_dir                     = 'simulations_NA_duration';
folder_fit                     = './fit_PT_met_SAEM';
dose_range                     = [60, 120, 240];
resection_times                = 27:1:45;
percent_relapse_overall_data   = 30;
flag_compute_control           = 2;
flag_compute                   = 2;
flag_plot_kinetics             = 1;
flag_plot_final_vs_NA_duration = 1;
flag_plot_legend               = 1;
flag_print_numbers             = 1;
for dose                       = dose_range
    simulate_surgery_vs_NA_f(...
        output_dir,...
        folder_fit,...
        dose,...
        resection_times,...
        percent_relapse_overall_data,...
        flag_compute,...
        flag_compute_control,...
        flag_plot_kinetics,...
        flag_plot_final_vs_NA_duration,...
        flag_plot_legend,...
        flag_print_numbers);
end
