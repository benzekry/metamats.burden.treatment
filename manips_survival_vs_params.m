clear all
close all
clc

constants;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import_data;
import_survival;
folder_fit     = './fit_PT_met_SAEM';
[param_inds, param_ind_d]  = compute_param_inds(...
    folder_fit,...
    subgroups,...
    [2,7:11],...
    subgroup_names,...
    1);
param_labels                = {
    'log(\mu) (mm^3)^{-1} \cdot day^{-1})'
    '\alpha (day^{-1})'
    '\beta (day^{-1})'
    'k ((mg/kg)^{-1})'};
param_names                 = {'log_mu', 'alpha', 'beta', 'k'};
subgroup_names = {
    'veh34'
    'Su60_3D'           % AB13_06, AB14_14
    'Su60_7D_ab_13_06'  % AB13_06
    'Su60_14D'          % AB12_11, AB13_06, AB14_14 
    'Su120_3D'          % AB12_11, AB14_14
    'Su120_3D_Su60_11D' % AB12_11, AB14_14
    };
treated_subgroups = {
    'Su60_3D'           % AB13_06, AB14_14
    'Su60_7D_ab_13_06'  % AB13_06
    'Su60_14D'          % AB12_11, AB13_06, AB14_14 
    'Su120_3D'          % AB12_11, AB14_14
    'Su120_3D_Su60_11D' % AB12_11, AB14_14
    };
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
folder_export  = './plots_surv/';
x_label        = 'Observed survival';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for p = 1:length(param_names)
    if ~exist([folder_export '/' param_names{p}], 'dir')
        mkdir([folder_export '/' param_names{p}])
    end
    switch p
        case {1, 2, 3} % mu, alpha, beta
            valid_subgroups = subgroup_names;
        case 4 % k
            valid_subgroups = treated_subgroups;
    end
    param_v     = param_ind_d.values(valid_subgroups);
    param_v     = vertcat(param_v{:});
    survivals_v = survival_d.values(valid_subgroups);
    survivals_v = vertcat(survivals_v{:});
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % All groups
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    plot_correlation(...
        survivals_v,...
        param_v(:, p),...
        x_label,...
        param_labels{p},...
        [folder_export '/' param_names{p} '/all_groups.pdf'])
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Loop over admissible groups
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for idx_subgroup = 1:length(valid_subgroups)
        param_group    = param_ind_d(valid_subgroups{idx_subgroup});
        survival_group = survival_d(valid_subgroups{idx_subgroup});
        plot_correlation(...
            survival_group,...
            param_group(:, p),...
            x_label,...
            param_labels{p},...
            [folder_export '/' param_names{p} '/' valid_subgroups{idx_subgroup} '.pdf'])
    end
end