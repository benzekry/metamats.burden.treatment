function [h_PT_model50, h_PT_model10, h_PT_model90,...
          h_met_model50, h_met_model10, h_met_model90] = ...
plot_PT_met_model_ther(...
    AX,...
    temps_PT,...
    temps_met,...
    pops_PT,...
    pops_met,...
    resection_time,...
    min_size,...
    max_size,...
    max_time,...
    flag_plot_met_distrib,...
    t_start,t_end,...
    file)
if ~exist('max_size', 'var')
    max_size = 1e10;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute percentiles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pop_PT_10  = prctile(pops_PT, 10);
pop_PT_50  = prctile(pops_PT, 50);
pop_PT_90  = prctile(pops_PT, 90);
pop_met_10 = prctile(pops_met, 10);
pop_met_50 = prctile(pops_met, 50);
pop_met_90 = prctile(pops_met, 90);
figure(1)
colors   = get(gca,'ColorOrder');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot PT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold(AX(1), 'on')
h_PT_model50       = plot(AX(1), temps_PT, pop_PT_50,...
    'Color', colors(1, :), 'Linewidth', 2);
h_PT_model10       = plot(AX(1), temps_PT, pop_PT_10, '--',...
    'Color', colors(1, :), 'Linewidth', 2);
h_PT_model90       = plot(AX(1), temps_PT, pop_PT_90, '--',...
    'Color', colors(1, :), 'Linewidth', 2);
hold(AX(1), 'off')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot Met
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if flag_plot_met_distrib == 1
    hold(AX(2), 'on')
    h_met_model50 = plot(AX(2), temps_met, pop_met_50,...
        'Color', colors(4, :), 'Linewidth', 2);
    h_met_model10 = plot(AX(2), temps_met, pop_met_10, '--',...
        'Color', colors(4, :), 'Linewidth', 2);
    h_met_model90 = plot(AX(2), temps_met, pop_met_90, '--',...
        'Color', colors(4, :), 'Linewidth', 2);
    hold(AX(2), 'off')
    hold off
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Axis labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ylabel(AX(1), 'Primary tumor size (cells)')
ylabel(AX(2), 'Metastatic burden (cells)')
min_size_noLog = 0;
max_size_noLog = 1e9;
set(AX(1), 'Ylim', [min_size_noLog, max_size_noLog])
set(AX(2), 'Ylim', [min_size_noLog, max_size_noLog])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Log scale
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(AX(1), 'Yscale', 'log', 'Ylim', [min_size, max_size])
set(AX(2), 'Yscale', 'log', 'Ylim', [min_size, max_size])
vis_state = get(AX(2), 'Visible');
if isequal(vis_state, 'on')
    set(AX(1), 'Units', 'pixels');
    set(AX(2), 'Units', 'pixels');
    hf = gcf;
    position = get(hf, 'Position');
    position(3) = position(3)*1.05;
    set(hf, 'Position', position);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cosmetics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot_lines_area(AX(1), resection_time)
plot_arrows_text_met(AX(1), resection_time, max_time)
if ~isnan(t_start)
    plot_line_arrow_therapy(AX(1), t_start, t_end)
end
uistack(AX(2), 'top')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Export
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('file', 'var')
      export_fig([file '.pdf']) 
end
end