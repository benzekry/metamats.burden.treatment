% Computes the metastatic mass via a first order quadrature
function [burden, nb] = ...
    model_burden_nb(...
    param,...               % metastatic parameters
    time,...                % time points
    growth_model_PT,...     % growth model for primary tumor
    growth_model_met,...    % growth model for metastases
    param_PT,...            % parameters for primary tumor growth (with resection time being the last one)
    flag_nb)                % flag that decides to compute number of mets or not
    global V0 Vinj gamma
    dt = 0.1;
    if max(time) > 1000
        dt = max(time)/500;
    end    
    resection_time = param_PT(end);
    mu             = param(1);        
    N              = 2^nextpow2(max(time)/dt);  %number of discretisation steps optimised for FFT
    T              = N*dt;
    t_vec          = dt:dt:T;    %calculation times
    loc            = int32(time/dt);
    if any(abs(time/dt - double(loc))>eps('single'))
        error(sprintf(['Time step too large: Observation times are not on grid. \n'...
            'Also check whether the discretization step in $time is in scale with'...
            'discretization step of the code']));
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of M (on 0:dt:T)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Vp      = growth_model_PT(param_PT(1:end-1), t_vec-dt, Vinj); % calculate Vp = X(t,V0,0) for convolution calculation
    X       = growth_model_met(param(2:end), t_vec, V0); % calculate X(t;V0,0) for convolution calculation            
    beta_xp = mu*Vp.^gamma.*((t_vec-dt) < resection_time);
    nb = zeros(1,N);
    if flag_nb == 1
        nb(1) = 0;
        for n = 2:N
            nb(n) = quadrat_1D_trap(beta_xp(1:n),dt);
        end
    end
    M       =  [0 euler_exp_FFT(X,beta_xp,N,T)];   % FFT-based explicit Euler quadrature
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
% Result
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    nb  =  nb(loc+1);
    if size(time,1) == 1
        burden  =  M(loc+1);
    elseif size(time,2) == 1
        burden  =  M(loc+1)';
    end
    if any(~isfinite(burden))
        warning('ModelNbBurden returns Inf value, model set to 0')
        burden = zeros(1,length(burden));
    end
%     if any(Burden<0)
%         warning('ModelNbBurden returns negative value, model set to 0')
%         Burden = zeros(1,length(Burden));
%     end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function u = euler_exp_FFT(f,g,N,T)    
    f_ext = [f zeros(1,N)];
    g_ext = [g zeros(1,N)];   
    Ff    = fft(f_ext);
    Fg    = fft(g_ext);    
    u_ext = (T/N)*ifft(Ff.*Fg);
    u     = u_ext(1:N);
end