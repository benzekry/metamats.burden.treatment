function V = model_ther_tIVI(param, time, t_start, k_e, Ci, Vc, tI, VI) % param: alpha0, beta, k. time > tI
alpha0 = param(1); % relative growth rate at size V0 = 1 cell
beta   = param(2);
k      = param(3);
M      = length(Ci); % note: length of Ci defines number of administrations and total treatment duration
Ci     = reshape(Ci, 1, M);
tau    = t_start:1:t_start + M - 1; % administration times (every day)
tau    = reshape(tau,size(Ci));

A      = zeros(size(M+1));
y0     = zeros(size(M+1));
y      = cell(M+1,1); 

% tI before ther
if tI <= tau(1)
    y0(1)             = log(VI/Vc);
    A(1)              = (beta * y0(1) - alpha0)/exp(-beta*tI);
    y{1}              = (A(1)*exp(-beta*time(time <= tau(1))) + alpha0)/beta;
    for m = 2:M
        D     = tau(m-1) + (k/k_e)*(Ci(1:m-2) * exp(-k_e * (tau(m-1) - tau(1:m-2)))');
        y0(m) = (alpha0 + A(m-1)*exp(-beta*D))/beta;    
        D     = tau(m-1) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (tau(m-1) - tau(1:m-1)))');
        A(m)  = (beta * y0(m) - alpha0)/exp(-beta*D);
        idx_m = find((time > tau(m-1)) & (time <= tau(m)));
        idx_m = reshape(idx_m, 1, length(idx_m));
        if ~isempty(idx_m)
            for n = idx_m
                D  = time(n) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (time(n) - tau(1:m-1)))');
                yy = (A(m)*exp(-beta*D) +alpha0)/beta;
                y{m} = [y{m}, yy];
            end
        end
    end
    D       = tau(M) + (k/k_e)*(Ci(1:M-1) * exp(-k_e * (tau(M) - tau(1:M-1)))');
    y0(M+1) = (alpha0 + A(M)*exp(-beta*D))/beta;        
    D       = tau(M) + (k/k_e)*(Ci(1:M) * exp(-k_e * (tau(M) - tau))');
    A(M+1)  = (beta * y0(M+1) - alpha0)/exp(-beta*D);
    idx_m   = find(time > tau(M));
    idx_m   = reshape(idx_m, 1, length(idx_m));
    if ~isempty(idx_m)
        for n = idx_m
            D  = time(n) + (k/k_e)*(Ci(1:M) * exp(-k_e * (time(n) - tau))');
            yy = (A(M+1)*exp(-beta*D) +alpha0)/beta;
            y{M+1} = [y{M+1}, yy];
        end
    end
% tI after ther    
elseif tau(M) <= tI 
    y0(M+1) = log(VI/Vc);
    D       = tI + (k/k_e)*(Ci(1:M) * exp(-k_e * (tI - tau))');
    A(M+1)  = (beta * y0(M+1) - alpha0)/exp(-beta*D);
    idx_m   = find(time > tI);
    idx_m   = reshape(idx_m, 1, length(idx_m));
    if ~isempty(idx_m)
        for n = idx_m
            D  = time(n) + (k/k_e)*(Ci(1:M) * exp(-k_e * (time(n) - tau))');
            yy = (A(M+1)*exp(-beta*D) +alpha0)/beta;
            y{M+1} = [y{M+1}, yy];
        end
    end
% tI during ther
else
    q       = max(find(tau < tI));
    y0(q+1) = log(VI/Vc);
    D       = tI + (k/k_e)*(Ci(1:q) * exp(-k_e * (tI - tau(1:q)))');
    A(q+1)  = (beta * y0(q+1) - alpha0)/exp(-beta*D);
    idx_m = find((time > tI) & (time <= tau(q+1)));
    idx_m = reshape(idx_m, 1, length(idx_m));
    if ~isempty(idx_m)
        for n = idx_m
            D  = time(n) + (k/k_e)*(Ci(1:q) * exp(-k_e * (time(n) - tau(1:q)))');
            yy = (A(q+1)*exp(-beta*D) +alpha0)/beta;
            y{q+1} = [y{q+1}, yy];
        end
    end
    for m = q+2:M
        D     = tau(m-1) + (k/k_e)*(Ci(1:m-2) * exp(-k_e * (tau(m-1) - tau(1:m-2)))');
        y0(m) = (alpha0 + A(m-1)*exp(-beta*D))/beta;    
        D     = tau(m-1) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (tau(m-1) - tau(1:m-1)))');
        A(m)  = (beta * y0(m) - alpha0)/exp(-beta*D);
        idx_m = find((time > tau(m-1)) & (time <= tau(m)));
        idx_m = reshape(idx_m, 1, length(idx_m));
        if ~isempty(idx_m)
            for n = idx_m
                D  = time(n) + (k/k_e)*(Ci(1:m-1) * exp(-k_e * (time(n) - tau(1:m-1)))');
                yy = (A(m)*exp(-beta*D) +alpha0)/beta;
                y{m} = [y{m}, yy];
            end
        end
    end
    D       = tau(M) + (k/k_e)*(Ci(1:M-1) * exp(-k_e * (tau(M) - tau(1:M-1)))');
    y0(M+1) = (alpha0 + A(M)*exp(-beta*D))/beta;        
    D       = tau(M) + (k/k_e)*(Ci(1:M) * exp(-k_e * (tau(M) - tau))');
    A(M+1)  = (beta * y0(M+1) - alpha0)/exp(-beta*D);
    idx_m   = find(time > tau(M));
    idx_m   = reshape(idx_m, 1, length(idx_m));
    if ~isempty(idx_m)
        for n = idx_m
            D  = time(n) + (k/k_e)*(Ci(1:M) * exp(-k_e * (time(n) - tau))');
            yy = (A(M+1)*exp(-beta*D) +alpha0)/beta;
            y{M+1} = [y{M+1}, yy];
        end
    end
end
V = [];
for p =1:M+1
    y{p} = reshape(y{p}, 1, length(y{p}));
    V = [V,y{p}];
end
V = Vc *exp(V);
if any(~isfinite(V))
    warning('Model returns Inf value, model set to 0')
    V = zeros(1,length(V));
end
V = reshape(V,size(time));
end
