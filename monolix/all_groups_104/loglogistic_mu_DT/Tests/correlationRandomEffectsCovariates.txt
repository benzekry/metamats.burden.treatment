eta,covariate,statistic,p-value,test
eta_Te,DT,0.0597371,0.968655,Pearson
eta_Te,g_p,1.63481,0.110105,Pearson
eta_Te,mu,0.00812816,0.998837,Pearson
eta_Te,tTrt,1.32987,0.267629,Fisher
eta_Te,trt,1.32987,0.267629,Fisher
