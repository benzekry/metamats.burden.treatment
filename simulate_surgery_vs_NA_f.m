function simulate_surgery_vs_NA_f(...
    output_dir,...
    folder_fit,...
    dose,...
    resection_times,...
    percent_relapse_overall_data,...
    flag_compute,...
    flag_compute_control,...
    flag_plot_kinetics,...
    flag_plot_final_vs_NA_duration,...
    flag_plot_legend,...
    flag_print_numbers)
constants;
if ~exist(output_dir, 'dir')
    mkdir(output_dir)
end
%--------------------------------------------------------------------------
%% Load data and build models
%--------------------------------------------------------------------------
% import_data;
build_models_met_PT;
% Parameters
fit_r               = load([folder_fit '/fit.mat']);
fixed_effects_PT    = fit_r.beta(2:4); % should be changed if the model is changed
covariance_PT       = fit_r.psi(2:4,2:4);
param_transform_PT  = fit_r.param_transform(2:4);
fixed_effects_met   = fit_r.beta(1:3);
covariance_met      = fit_r.psi(1:3,1:3);
param_PT            = exp(fixed_effects_PT);
param_met           = exp(fixed_effects_met);
param_transform_met = fit_r.param_transform(1:3);
conv_unit_PT        = mm2cell;
conv_unit_met       = phot2cell;
NA_durations        = resection_times - resection_times(1);
nb_NA               = length(resection_times);
t_start             = resection_times(1);
max_time            = 85;
nb_simu_control     = 1000;
nb_simu             = 1000;
%--------------------------------------------------------------------------
%% Cosmetics
%--------------------------------------------------------------------------
min_PT_percent      = -110;
max_PT_percent      = 50;
min_MFS             = 28;
max_MFS             = 55;
max_size            = 1.2e9;
min_size            = 1e8;
min_MB              = 2.5e8; % for plot of final MB vs resection_times
max_MB              = 7.5e8;
cmap_name           = 'copper';
cmap                = flipud(colormap(cmap_name));
fontaxes            = 18;
font_size           = 20;
line_style_MFS      = '-o';
marker_style_MFS    = 'o';
color_MFS           = 'k';
options.color_area = [0.85, 0.85, 0.85]; % area for final PT
options.color_line = [0, 0, 0]; % color for final PT
%--------------------------------------------------------------------------
%% Simulate population without NA
%--------------------------------------------------------------------------
resection_time      = resection_times(1);
temps_PT            = 0:0.1:resection_time;
temps_met           = 0:0.1:max_time;
model_PT_f          = @(param, time) gompertz_exp_alpha0_beta(param, DT_invitro, time, cell2mm(1), Vinj);
model_met_f         = @(param, time, param_PT_with_resec_time) model_burden_nb(...
    [param(1),  param_PT_with_resec_time],...
    time,...
    @(param, time, X0) gompertz_exp_alpha0_beta(...
    param,...
    DT_invitro,...
    time,...
    cell2mm(1),...
    X0), ...
    @(param, time, X0) gompertz_exp_alpha0_beta(...
    param,...
    DT_invitro,...
    time,...
    cell2phot(1),...
    X0),...
    param_PT_with_resec_time,...
    0);
if flag_compute_control == 1
    [pops_PT_control, pops_met_control] = simulate_pop_PT_met(...
        temps_PT,...
        temps_met,...
        model_PT_f,...
        model_met_f,...
        fixed_effects_PT,...    % fixed effects of the PT parameters
        covariance_PT,...       % covariance of the random effects for the PT parameters
        param_transform_PT,...
        fixed_effects_met,...
        covariance_met,...            % covariance of the random effects for the met parameters
        param_transform_met,...
        resection_time,...
        nb_simu_control);                  % number of simulations
    save([output_dir '/simu_control'])
elseif flag_compute_control == 2
    load([output_dir '/simu_control'], 'pops_PT_control', 'pops_met_control')
end
PT_control  = model_PT_f(param_PT, temps_PT);
met_control = model_met_f(param_met, temps_met, [param_PT', resection_time]);
% Get MB threshold corresponding to percent_relapse_overall_data
MB_threshold = prctile(pops_met_control(:, end), 100 - percent_relapse_overall_data); % in p/s
%--------------------------------------------------------------------------
%% Simulate various NA durations
%--------------------------------------------------------------------------
temps_PTs   = cell(1, nb_NA);
PTs         = cell(1, nb_NA);
mets        = cell(1, nb_NA);
pop_PTs     = cell(1, nb_NA);
pop_mets    = cell(1, nb_NA);
if flag_compute == 1
    for idx = 2:nb_NA
        resection_time = resection_times(idx);
        display(resection_time)
        doses          = dose*ones(1, resection_time - t_start);
        model_PT_f     = @(param, time) GompExp_ther(...
            param,...
            time,...
            t_start,...
            DT_invitro,...
            k_e,...
            doses,...
            cell2mm(1),...
            Vinj);
        temps_PT       = 0:0.1:resection_time;
        temps_PTs{idx} = temps_PT;
        model_met_f    = @(param_met, time, param_PT_with_resec_time) model_burden_nb(...
            param_met,...
            time,...
            @(param, time, X0) GompExp_ther(...
            param,...
            time,...
            t_start,...
            DT_invitro,...
            k_e,...
            doses,...
            cell2mm(1),...
            X0),...
            @(param, time, X0) gompertz_exp_alpha0_beta(...
            param,...
            DT_invitro,...
            time,...
            cell2phot(1),...
            X0),...
            param_PT_with_resec_time,...
            0);
        [pop_PT, pop_met] = simulate_pop_PT_met(...
            temps_PT,...
            temps_met,...
            model_PT_f,...
            model_met_f,...
            fixed_effects_PT,...    % fixed effects of the PT parameters
            covariance_PT,...       % covariance of the random effects for the PT parameters
            param_transform_PT,...
            fixed_effects_met,...
            covariance_met,...            % covariance of the random effects for the met parameters
            param_transform_met,...
            resection_times(idx),...
            nb_simu);                  % number of simulations
        pop_PTs{idx}  = pop_PT;
        pop_mets{idx} = pop_met;
        PTs{idx}  = model_PT_f(param_PT, temps_PT);
        mets{idx} = model_met_f(param_met, temps_met, [param_PT', resection_time]);
    end
    save([output_dir '/simu_dose_' num2str(dose)])
elseif flag_compute == 2
    load([output_dir '/simu_dose_' num2str(dose)], ...
        'temps_PTs', 'pop_PTs', 'pop_mets', 'PTs', 'mets', 'nb_simu')
end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% Plots and outputs
%--------------------------------------------------------------------------
pop_PTs{1}  = pops_PT_control;
pop_mets{1} = pops_met_control;
PTs{1}      = PT_control;
mets{1}     = met_control;
%--------------------------------------------------------------------------
%% Size kinetics
%--------------------------------------------------------------------------
if flag_plot_kinetics == 1
    close all
    figure(1)
    clf
    hold on
    plot(temps_PT, conv_unit_PT(PT_control), 'color', cmap(1, :), 'Linewidth', 2);
    plot(temps_met, conv_unit_met(met_control), 'color', cmap(1, :), 'Linewidth', 2);
    end_idx = length(temps_PT);
    for idx = round(linspace(5, nb_NA, 4))
        idx_cmap = floor(idx/nb_NA*size(cmap, 1));        
        plot(temps_PTs{idx}(end_idx:end), conv_unit_PT(PTs{idx}(end_idx:end)), 'color', cmap(idx_cmap, :), 'Linewidth', 2); % colors(1, :))
        plot(temps_met(end_idx:end), conv_unit_met(mets{idx}(end_idx:end)), 'color', cmap(idx_cmap, :), 'Linewidth', 2); % colors(2, :))
        xline(max(temps_PTs{idx}), '--', 'color', cmap(idx_cmap, :), 'Linewidth', 2)
        end_idx = length(temps_PTs{idx});
    end
    hold off
    ax = gca;
    set(ax, 'yscale', 'log');
    xlabel('Time (days)', 'Fontsize', font_size, 'Fontweight', 'bold');
    ylabel('Tumor burden (cells)', 'Fontsize', font_size, 'Fontweight', 'bold');
    set(ax, 'Fontsize', fontaxes)
    set(ax, 'ylim', [min_size, max_size])
    plot_lines_area(ax, resection_times(end))
    plot_arrows_text_met(ax, resection_times(end), max_time)
    plot_line_arrow_therapy(ax, resection_times(1), resection_times(end), false)
    xline(resection_times(1), '--', 'color', cmap(1, :), 'Linewidth', 2)
    export_fig([output_dir '/growth_kinetics_dose_' num2str(dose) '.pdf']);
    % Colorbar
    figure(99)
    clf
    ax = axes;
    set(ax, 'fontsize', fontaxes)
    colormap(cmap_name);
    cb            = colorbar(ax, 'Direction', 'reverse');
    cb.Ticks      = (NA_durations - NA_durations(1))/(NA_durations(end) - NA_durations(1));
    cb.TickLabels = fliplr(NA_durations);
    ax.Visible = 'off';
    export_fig([output_dir '/colorbar_growth_kinetics.pdf']);    
end
%--------------------------------------------------------------------------
%% Print numbers
%--------------------------------------------------------------------------
if flag_print_numbers == 1
    display('---------------------------------------------------------')
    display(sprintf('Dose level = %d', dose))
    display(sprintf('Final MB for NA = %g: %.3g cells', [NA_durations(1), conv_unit_met(mets{1}(end))]))
    display(sprintf('Final MB for NA = %g: %.3g cells', [NA_durations(end), conv_unit_met(mets{end}(end))]))
    display(sprintf('Increase = %.3g %%', (conv_unit_met(mets{end}(end)) - conv_unit_met(mets{1}(end)))/conv_unit_met(mets{1}(end))*100))
end
%--------------------------------------------------------------------------
%% Final PT value and metastasis free survival vs NA_durations
%--------------------------------------------------------------------------
if flag_plot_final_vs_NA_duration == 1
    %% Computations
    idx_start                   = find(temps_PT >= t_start, 1, 'first');
    percent_reduction_PTs       = zeros(nb_simu, nb_NA);
    percent_reduction_PTs(:, 1) = 0;
    met_ends                    = zeros(1, nb_NA);
    MFSs                        = zeros(1, nb_NA);
    MFSs(1)                     = percent_relapse_overall_data;
    for idx = 2:nb_NA
        percent_reduction_PTs(:, idx) = 100*(pop_PTs{idx}(:, end) - pop_PTs{idx}(:, idx_start))./pop_PTs{idx}(:, idx_start);
        met_ends(idx)                 = conv_unit_met(mets{idx}(end));
        MFSs(idx)                     = 100*sum(pop_mets{idx}(:, end) > MB_threshold)/nb_simu;
    end
    % Graphics
    hf_2 = figure(2);
    clf
    %% Generate two axes
    [AX, H1, H2] = plotyy(NA_durations(1), percent_reduction_PTs(1), NA_durations(1), MFSs(1));
    set(H1, 'LineStyle', 'none', 'Marker', 'none');
    set(H2, 'LineStyle', 'none', 'Marker', 'none');
    set(AX(1), 'xlim', [NA_durations(1), NA_durations(end)]);
    set(AX(2), 'xlim', [NA_durations(1), NA_durations(end)]);
    set(AX(1), 'ylim', [min_PT_percent, max_PT_percent]);
    set(AX(2), 'ylim', [min_MFS, max_MFS]);
    set(AX, {'ycolor'}, {'k';'k'});
    set(AX, {'Xticklabel'}, {[];[]});
    set(AX, {'Xtick'}, {[];[]});
    set(AX, {'Yticklabel'}, {[];[]});
    set(AX, {'Ytick'}, {[];[]});
    hold(AX(1), 'on')
    hold(AX(2), 'on')
    %% Plot PT size percent change VS NA duration
    axes(AX(1))
    options = struct();
    options.color_area = [0.85, 0.85, 0.85];
    options.color_line = [0, 0, 0];
    options.axis       = AX(1);
    plot_area(...
        NA_durations,...
        prctile(percent_reduction_PTs, 50),...
        prctile(percent_reduction_PTs, 10),...
        prctile(percent_reduction_PTs, 90),...
        options);
    %% Plot MB change VS NA duration
    axes(AX(2))
    % p = plot(AX(2), NA_durations(2:end), met_ends(2:end), 'Linewidth', 2);
    p = plot(AX(2), NA_durations, MFSs, line_style_MFS, 'Linewidth', 2, 'Color', color_MFS);
    set(gca, 'Fontsize', fontaxes)
    % drawnow
    % n  = length(NA_durations) - 1;
    % cd = [uint8(flipud(copper(n)*255)) uint8(ones(n,1))]';
    % set(p.Edge, 'ColorBinding','interpolated', 'ColorData', cd)
    %% Cosmetics
    set(AX(1), 'XtickMode', 'auto')
    set(AX(1), 'XticklabelMode', 'auto')
    set(AX(1), 'YtickMode', 'auto')
    set(AX(1), 'YtickLabelMode', 'auto')
    set(AX(1), 'box', 'off')
    set(AX(2), 'YtickMode', 'auto');
    set(AX(2), 'YtickLabelMode', 'auto')
    set(AX(1), 'Fontsize', fontaxes)
    set(AX(2), 'Fontsize', fontaxes)
    xlabel(AX(1), 'Tx duration (days)', 'Fontsize', font_size, 'Fontweight', 'bold')
    ylabel(AX(1), 'Percent change in PT size', 'Fontsize', font_size, 'Fontweight', 'bold')
    ylabel(AX(2), 'Probability of metastatic relapse (%)', 'Fontsize', font_size, 'Fontweight', 'bold')
    hf_2.Position(3) = 620; % ylabel2 not showing... has to enlarge globally the plot
    % set(gca, 'Ylim', [min_MB, max_MB])
    %% Export
    export_fig([output_dir '/final_vs_NA_duration_dose_' num2str(dose) '.png'], '-r300');
end
%% Legend
if flag_plot_legend == 1
    hf = figure(4);
    clf
    ha = axes();
    h = plot(1, 1, '-', 'Linewidth', 2, 'Color', options.color_line);
    h.Visible = 'off';
    hold on
    patch = fill(...
        [0.2, 0.6, 0.6, 0.2],...
        [0, 0, 0.5, 0.5],...
        options.color_area);
    set(patch, 'edgecolor', 'none', 'FaceAlpha', 0.5, 'Visible', 'off');
    h = plot(1, 1, line_style_MFS, 'Linewidth', 2, 'Color', color_MFS);
    h.Visible = 'off';
    hold off
    ha.Visible  = 'off';
    Legende     = {'Primary tumor', '80% of population', 'Probability of metastatic relapse'};
    h_leg       = legend(Legende);
    set(h_leg, 'EdgeColor', 'w')
    set(hf, 'Color', 'w')
    export_fig([output_dir '/final_vs_NA_duration_legend.png'], '-r500')
end
close('all')